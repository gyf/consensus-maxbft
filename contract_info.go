/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"

//peer defines basic peer information required by consensus
type peer struct {
	id    string //The network id
	index int64  //The index of committee
}

type contractInfo struct {
	*committee
	*consensuspb.GovernanceContract
}

func newContractInfo(governContract *consensuspb.GovernanceContract) *contractInfo {
	peers := make([]*peer, 0, len(governContract.Validators))
	lastPeers := make([]*peer, 0, len(governContract.LastValidators))
	for _, validator := range governContract.Validators {
		peers = append(peers, &peer{
			id:    validator.NodeId,
			index: validator.Index,
		})
	}
	for _, validator := range governContract.LastValidators {
		lastPeers = append(lastPeers, &peer{
			id:    validator.NodeId,
			index: validator.Index,
		})
	}
	committee := newCommittee(peers, lastPeers, governContract.NextSwitchHeight,
		governContract.MinQuorumForQc, governContract.LastMinQuorumForQc, governContract.EpochId)
	return &contractInfo{committee: committee, GovernanceContract: governContract}
}

//committee manages all of peers join current consensus epoch
type committee struct {
	switchEpochHeight uint64

	// last epoch info
	lastValidators     []*peer
	lastMinQuorumForQc int

	// curr epoch info
	epochId            uint64
	validators         []*peer // Consensus nodes at current epoch
	currMinQuorumForQc int
}

//newCommittee initializes a peer pool with given peer list
func newCommittee(peers, lastValidators []*peer,
	switchHeight uint64, quorumQc, lastQuorumQc, epochId uint64) *committee {
	return &committee{
		validators:         peers,
		lastValidators:     lastValidators,
		switchEpochHeight:  switchHeight,
		currMinQuorumForQc: int(quorumQc),
		lastMinQuorumForQc: int(lastQuorumQc),
		epochId:            epochId,
	}
}

//getPeers returns peer list which are online
func (pp *committee) getPeers(blkHeight uint64) []*peer {
	usedPeers := pp.getUsedPeers(blkHeight)
	return usedPeers
}

//getPeerByIndex returns a peer with given index
func (pp *committee) getPeerByIndex(height uint64, index uint64) *peer {
	usedPeers := pp.getUsedPeers(height)
	for _, v := range usedPeers {
		if v.index == int64(index) {
			return v
		}
	}
	return nil
}

//isValidIdx checks whether a index is valid
func (pp *committee) isValidIdx(index uint64, height uint64) bool {
	usedPeers := pp.getUsedPeers(height)
	for _, v := range usedPeers {
		if v.index == int64(index) {
			return true
		}
	}
	return false
}

// minQuorumForQc The Quorum is obtained according to the height of the block
func (pp *committee) minQuorumForQc(height uint64) int {
	// blk1 <- blk2 <- blk3 <- blk4 <- blk5
	// blk1 contain epoch switch info
	// blk4 will commit blk1 by threeChain rules in consensus node
	// So blk4 is a critical region in epoch switching logic, and blk4
	// will verify by last validators and quorum
	if pp.switchEpochHeight != 0 && height > pp.switchEpochHeight && height <= pp.switchEpochHeight+3 {
		return pp.lastMinQuorumForQc
	}
	return pp.currMinQuorumForQc
}

// getUsedPeers The validators is obtained according to the height of the block
func (pp *committee) getUsedPeers(height uint64) []*peer {
	usedPeers := pp.validators
	// review comments in minQuorumForQc func
	if pp.selectPrevious(height) {
		usedPeers = pp.lastValidators
	}
	return usedPeers
}

func (pp *committee) getEpochId(height uint64) uint64 {
	if pp.selectPrevious(height) {
		return pp.epochId - 1
	}
	return pp.epochId
}

func (pp *committee) selectPrevious(height uint64) bool {
	return pp.switchEpochHeight != 0 && height > pp.switchEpochHeight && height <= pp.switchEpochHeight+3
}
