/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"chainmaker.org/chainmaker/consensus-maxbft/v2/governance"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/message"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/utils"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
)

//epochManager manages the components that shared across epoch
type epochManager struct {
	index        uint64 //Local assigned index in next committee group
	epochId      uint64
	createHeight uint64
	createLevel  uint64
	switchHeight uint64 //real switch epoch

	msgPool            *message.MsgPool //The msg pool associated to next epoch
	governanceContract *consensuspb.GovernanceContract
}

// createNextEpochIfRequired If the conditions are met, create the next epoch
func (cbi *ConsensusMaxBftImpl) createNextEpochIfRequired(height, commitLevel uint64) (*epochManager, error) {
	governContract, err := governance.NewGovernanceContract(cbi.store, cbi.ledgerCache).GetGovernanceContract()
	if err != nil {
		return nil, err
	}

	cbi.logger.Debugf("begin createNextEpochIfRequired, "+
		"contractEpoch:%d, nodeEpoch:%d", governContract.EpochId, cbi.smr.paceMaker.GetEpochId())
	if governContract.EpochId == cbi.smr.paceMaker.GetEpochId() {
		return nil, nil
	}
	epoch, err := cbi.createEpoch(height, commitLevel, governContract)
	cbi.logger.Debugf("end createNextEpochIfRequired")
	return epoch, err
}

// createEpoch create the epoch in the block height
func (cbi *ConsensusMaxBftImpl) createEpoch(
	height, commitLevel uint64, govContract *consensuspb.GovernanceContract) (*epochManager, error) {

	epoch := &epochManager{
		index:        utils.InvalidIndex,
		createHeight: height,
		createLevel:  commitLevel,

		epochId:            govContract.EpochId,
		switchHeight:       govContract.NextSwitchHeight,
		governanceContract: govContract,
		msgPool: message.NewMsgPool(govContract.GetCachedLen(),
			int(govContract.GetValidatorNum()), int(govContract.MinQuorumForQc)),
	}
	for _, v := range govContract.Validators {
		if v.NodeId == cbi.id {
			epoch.index = uint64(v.Index)
			break
		}
	}
	return epoch, nil
}

//isValidProposer checks whether given index is valid at level
func (cbi *ConsensusMaxBftImpl) isValidProposer(height, level, index uint64) bool {
	proposerIndex := cbi.getProposer(height, level)
	return proposerIndex == index
}

func (cbi *ConsensusMaxBftImpl) getProposer(height, level uint64) uint64 {
	contractInfo := cbi.smr.info
	validators := cbi.smr.getPeers(height)
	if len(validators) == 0 {
		return utils.InvalidIndex
	}
	index := (level / contractInfo.GetNodeProposeRound()) % uint64(len(validators))
	return uint64(validators[index].index)
}
