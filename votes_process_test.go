/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"fmt"
	"runtime"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/panjf2000/ants/v2"
)

func TestForRange(t *testing.T) {
	arr := make([]*int, 0, 4)
	for i := 0; i < cap(arr); i++ {
		v := i + 1
		arr = append(arr, &v)
	}

	verifyPool, err := ants.NewPool(runtime.NumCPU(), ants.WithPreAlloc(true))
	require.NoError(t, err)
	defer verifyPool.Release()

	wg := sync.WaitGroup{}
	wg.Add(len(arr))
	for _, v := range arr {
		require.NoError(t, verifyPool.Submit(func() {
			defer wg.Done()
			fmt.Printf("first iter %p\n", v)
		}))
	}
	wg.Wait()
	fmt.Printf("\n")

	wg.Add(len(arr))
	for i := range arr {
		require.NoError(t, verifyPool.Submit(func() {
			defer wg.Done()
			fmt.Printf("second iter %p\n", arr[i])
		}))
	}
	wg.Wait()

	fmt.Printf("\n")

	wg.Add(len(arr))
	for i := range arr {
		index := i
		require.NoError(t, verifyPool.Submit(func() {
			defer wg.Done()
			fmt.Printf("three iter %p\n", arr[index])
		}))
	}
	wg.Wait()

}
