/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"os"
	"os/exec"
	"testing"
	"time"

	"github.com/golang/mock/gomock"

	consensus_utils "chainmaker.org/chainmaker/consensus-utils/v2"
	"chainmaker.org/chainmaker/consensus-utils/v2/testframework"
	"chainmaker.org/chainmaker/logger/v2"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	"chainmaker.org/chainmaker/protocol/v2"
	"github.com/stretchr/testify/require"
)

var (
	blockchainId     = "chain1"
	nodeNums         = 4
	ConsensusEngines = make([]protocol.ConsensusEngine, nodeNums)
	CoreEngines      = make([]protocol.CoreEngine, nodeNums)
	consensusType    = consensuspb.ConsensusType_MAXBFT
)

func TestMaxBFTFramework(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	err := os.RemoveAll("chain1")
	require.Nil(t, err)
	cmd := exec.Command("/bin/sh", "-c", "rm -rf chain1 default.*")
	err = cmd.Run()
	require.Nil(t, err)

	err = testframework.InitLocalConfigs()
	require.Nil(t, err)
	//defer testframework.RemoveLocalConfigs()

	testframework.SetTxSizeAndTxNum(2*1024, 20*1024)

	// init LocalConfig
	testframework.InitLocalConfig(nodeNums)

	//create test_node_configs
	testNodeConfigs, err := testframework.CreateTestNodeConfig(ctrl, nodeNums, blockchainId, consensusType, nil)
	if err != nil {
		t.Errorf("%v", err)
	}

	for i := 0; i < nodeNums; i++ {

		// new CoreEngine
		CoreEngines[i] = testframework.NewCoreEngineForTest(testNodeConfigs[i], logger.GetLogger(logger.MODULE_CORE))

		tc := &consensus_utils.ConsensusImplConfig{
			ChainId:       testNodeConfigs[i].ChainID,
			NodeId:        testNodeConfigs[i].NodeId,
			Ac:            testNodeConfigs[i].Ac,
			Core:          CoreEngines[i],
			ChainConf:     testNodeConfigs[i].ChainConf,
			Signer:        testNodeConfigs[i].Signer,
			LedgerCache:   testNodeConfigs[i].LedgerCache,
			MsgBus:        testNodeConfigs[i].MsgBus,
			Store:         testNodeConfigs[i].BlockchainStore,
			ProposalCache: testNodeConfigs[i].ProposalCache,
		}

		consensus, errs := New(tc)
		if errs != nil {
			t.Errorf("%v", errs)
		}

		ConsensusEngines[i] = consensus
	}

	tf, err := testframework.NewTestClusterFramework(blockchainId,
		consensusType, nodeNums, testNodeConfigs, ConsensusEngines, CoreEngines)
	require.Nil(t, err)

	l := &logger.LogConfig{
		SystemLog: logger.LogNodeConfig{
			FilePath:        "./default.log",
			LogLevelDefault: "DEBUG",
			LogLevels:       map[string]string{"consensus": "DEBUG", "core": "DEBUG", "net": "DEBUG"},
			LogInConsole:    true,
			ShowColor:       true,
		},
	}
	logger.SetLogConfig(l)

	tf.Start()
	time.Sleep(10 * time.Second)
	tf.Stop()
}
