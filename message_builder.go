/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"bytes"

	"chainmaker.org/chainmaker/consensus-maxbft/v2/governance"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/utils"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"github.com/gogo/protobuf/proto"
)

//constructBlock generates a block at height and level
func (cbi *ConsensusMaxBftImpl) constructBlock(block *common.Block, level uint64) *common.Block {
	var (
		err     error
		txRWSet *common.TxRWSet
	)
	if txRWSet, err = governance.CheckAndCreateGovernmentArgs(cbi.proposalCache, block,
		cbi.governanceContract); err != nil {
		cbi.logger.Errorf("CheckAndCreateGovernmentArgs error:%+v", err)
		return nil
	}
	if err = utils.AddConsensusArgstoBlock(block, level, txRWSet); err != nil {
		cbi.logger.Errorf(`add consensus args to block err, %v`, err)
		return nil
	}
	if err = utils.SignBlock(block, cbi.chainConf.ChainConfig().Crypto.Hash, cbi.singer); err != nil {
		cbi.logger.Errorf(`sign block err, %v`, err)
		return nil
	}
	return block
}

//generateProposal generates a proposal at height and level
func (cbi *ConsensusMaxBftImpl) constructProposal(
	block *common.Block, level uint64, epochId uint64) *maxbft.ConsensusPayload {
	qc := cbi.chainStore.getCurrentQC()
	if qc.Height != block.Header.BlockHeight-1 || !bytes.Equal(block.Header.PreBlockHash, qc.BlockId) {
		cbi.logger.Warnf("preBlock qc's[%d:%x] not found in consensus, need to"+
			" wait from the votes", block.Header.BlockHeight-1, block.Header.PreBlockHash)
		return nil
	}
	toProposalBlock := cbi.constructBlock(block, level)
	if toProposalBlock == nil {
		return nil
	}
	proposalData := &maxbft.ProposalData{
		Level:       level,
		Height:      block.Header.BlockHeight,
		EpochId:     epochId,
		Block:       toProposalBlock,
		Proposer:    []byte(cbi.id),
		ProposerIdx: cbi.selfIndexInEpoch,
		JustifyQc:   qc,
	}
	syncInfo := &maxbft.SyncInfo{
		HighestQc:      qc,
		HighestTc:      cbi.smr.getTC(),
		HighestTcLevel: cbi.smr.getHighestTCLevel(),
	}
	proposalMsg := &maxbft.ProposalMsg{
		SyncInfo:     syncInfo,
		ProposalData: proposalData,
	}
	cbi.logger.Debugf("service selfIndexInEpoch [%v] constructProposal, proposal: [%v:%v:%v],"+
		"proposalMsgSize: %d", cbi.selfIndexInEpoch, proposalData.ProposerIdx,
		proposalData.Height, proposalData.Level, proposalMsg.Size())

	consensusPayload := &maxbft.ConsensusPayload{
		Type: maxbft.MessageType_PROPOSAL_MESSAGE,
		Data: &maxbft.ConsensusPayload_ProposalMsg{
			ProposalMsg: proposalMsg},
	}
	return consensusPayload
}

//constructVote builds a vote msg with given params
func (cbi *ConsensusMaxBftImpl) constructVote(height uint64, level uint64, epochId uint64,
	block *common.Block) (*maxbft.ConsensusPayload, error) {
	voteData := &maxbft.VoteData{
		Level:     level,
		Height:    height,
		EpochId:   epochId,
		Author:    []byte(cbi.id),
		NewView:   false,
		AuthorIdx: cbi.selfIndexInEpoch,
	}
	if block == nil {
		voteData.NewView = true
	} else {
		voteData.BlockId = block.Header.BlockHash
	}
	var (
		err  error
		data []byte
		sign []byte
	)
	if data, err = proto.Marshal(voteData); err != nil {
		return nil, err
	}
	if sign, err = cbi.singer.Sign(cbi.chainConf.ChainConfig().Crypto.Hash, data); err != nil {
		cbi.logger.Errorf("sign data failed, err %v data %v", err, data)
		return nil, err
	}

	voteData.Signature = &common.EndorsementEntry{
		Signer:    nil,
		Signature: sign,
	}
	syncInfo := &maxbft.SyncInfo{
		HighestTc:      cbi.smr.getTC(),
		HighestQc:      cbi.chainStore.getCurrentQC(),
		HighestTcLevel: cbi.smr.getHighestTCLevel(),
	}
	vote := &maxbft.VoteMsg{
		VoteData: voteData,
		SyncInfo: syncInfo,
	}
	consensusPayload := &maxbft.ConsensusPayload{
		Type: maxbft.MessageType_VOTE_MESSAGE,
		Data: &maxbft.ConsensusPayload_VoteMsg{
			VoteMsg: vote},
	}
	return consensusPayload, nil
}

//constructBlockFetchMsg builds a block fetch request msg at given height
func (cbi *ConsensusMaxBftImpl) constructBlockFetchMsg(reqID uint64, endBlockId []byte,
	endHeight uint64, num uint64, commitBlock, lockedBlock []byte) *maxbft.ConsensusPayload {
	msg := &maxbft.BlockFetchMsg{
		ReqId:       reqID,
		Height:      endHeight,
		BlockId:     endBlockId,
		NumBlocks:   num,
		AuthorIdx:   cbi.selfIndexInEpoch,
		CommitBlock: commitBlock,
		LockedBLock: lockedBlock,
	}
	consensusPayload := &maxbft.ConsensusPayload{
		Type: maxbft.MessageType_BLOCK_FETCH_MESSAGE,
		Data: &maxbft.ConsensusPayload_BlockFetchMsg{
			BlockFetchMsg: msg},
	}
	return consensusPayload
}

//constructBlockFetchRespMsg builds a blßock fetch response with given params
func (cbi *ConsensusMaxBftImpl) constructBlockFetchRespMsg(blocks []*maxbft.BlockPair,
	status maxbft.BlockFetchStatus, respId uint64) *maxbft.ConsensusPayload {
	msg := &maxbft.BlockFetchRespMsg{
		RespId:    respId,
		Status:    status,
		Blocks:    blocks,
		AuthorIdx: cbi.selfIndexInEpoch,
	}
	consensusPayload := &maxbft.ConsensusPayload{
		Type: maxbft.MessageType_BLOCK_FETCH_RESP_MESSAGE,
		Data: &maxbft.ConsensusPayload_BlockFetchRespMsg{
			BlockFetchRespMsg: msg},
	}
	return consensusPayload
}
