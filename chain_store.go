/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package maxbft

import (
	"bytes"
	"fmt"

	commonErrors "chainmaker.org/chainmaker/common/v2/errors"
	"chainmaker.org/chainmaker/common/v2/msgbus"
	blockpool "chainmaker.org/chainmaker/consensus-maxbft/v2/block_pool"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/utils"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/protocol/v2"

	"github.com/gogo/protobuf/proto"
)

// Access data on the chain and in the cache, commit block data on the chain
type chainStore struct {
	logger          protocol.Logger
	blockChainStore protocol.BlockchainStore // Provide information queries on the chain
	blockCommitter  protocol.BlockCommitter

	commitLevel      uint64               // The latest block level on the chain
	commitHeight     uint64               // The latest block height on the chain
	commitQuorumCert *maxbftpb.QuorumCert // The latest committed QC on the chain

	msgbus    msgbus.MessageBus
	blockPool *blockpool.BlockPool // Cache block and QC information
}

func initChainStore(server *ConsensusMaxBftImpl) (*chainStore, error) {
	bestBlock := server.ledgerCache.GetLastCommittedBlock()
	if bestBlock.Header.BlockHeight == 0 {
		if err := initGenesisBlock(bestBlock); err != nil {
			return nil, err
		}
	}
	bestBlkQCBz := utils.GetQCFromBlock(bestBlock)
	if len(bestBlkQCBz) == 0 {
		return nil, fmt.Errorf("get qc from block failed [%d:%x]", bestBlock.Header.BlockHeight, bestBlock.Header.BlockHash)
	}
	bestBlkQC := &maxbftpb.QuorumCert{}
	if err := proto.Unmarshal(bestBlkQCBz, bestBlkQC); err != nil {
		return nil, err
	}
	chainStore := &chainStore{
		logger:           server.logger,
		blockChainStore:  server.store,
		blockCommitter:   server.blockCommitter,
		commitLevel:      bestBlkQC.GetLevel(),
		commitHeight:     bestBlock.GetHeader().GetBlockHeight(),
		commitQuorumCert: bestBlkQC,
		blockPool:        blockpool.NewBlockPool(bestBlock, bestBlkQC),
		msgbus:           server.msgbus,
	}
	chainStore.logger.Debugf("init chainStore by bestBlock, height: %d, hash: %x",
		bestBlock.Header.BlockHeight, bestBlock.Header.BlockHash)
	return chainStore, nil
}

func initGenesisBlock(block *common.Block) error {
	qcForGenesis := &maxbftpb.QuorumCert{
		Votes:   []*maxbftpb.VoteData{},
		BlockId: block.Header.BlockHash,
	}
	qcData, err := proto.Marshal(qcForGenesis)
	if err != nil {
		return fmt.Errorf("openChainStore failed, marshal genesis qc, err %v", err)
	}
	if err = utils.AddQCtoBlock(block, qcData); err != nil {
		return fmt.Errorf("openChainStore failed, add genesis qc, err %v", err)
	}
	if err = utils.AddConsensusArgstoBlock(block, 0, nil); err != nil {
		return fmt.Errorf("openChainStore failed, add genesis args, err %v", err)
	}
	return nil
}

func (cs *chainStore) updateCommitCacheInfo(bestBlock *common.Block) error {
	qc := cs.blockPool.GetQCByID(string(bestBlock.Header.BlockHash))
	if qc == nil {

		cs.logger.Infof("not find committed block's qc from block pool[%d:%x]",
			bestBlock.Header.BlockHeight, bestBlock.Header.BlockHash)
		qc = new(maxbftpb.QuorumCert)
		qcData := utils.GetQCFromBlock(bestBlock)
		if len(qcData) == 0 {
			return fmt.Errorf("not find committed block's qc from block[%d:%x]",
				bestBlock.Header.BlockHeight, bestBlock.Header.BlockHash)
		}
		err := proto.Unmarshal(qcData, qc)
		if err != nil {
			return fmt.Errorf("unmarshal qc failed. qc:%+v, err:%+v", qcData, err)
		}
	}
	cs.commitLevel = qc.GetLevel()
	cs.commitHeight = bestBlock.GetHeader().GetBlockHeight()
	cs.commitQuorumCert = qc
	return nil
}

//func (cs *chainStore) getCommitQC() *maxbftpb.QuorumCert {
//	return cs.commitQuorumCert
//}

func (cs *chainStore) getCommitHeight() uint64 {
	return cs.commitHeight
}

func (cs *chainStore) getCommitLevel() uint64 {
	return cs.commitLevel
}

func (cs *chainStore) insertBlock(block *common.Block, curLevel uint64) error {
	if block == nil {
		return fmt.Errorf("insertBlock failed, nil block")
	}
	if exist := cs.blockPool.GetBlockByID(string(block.GetHeader().GetBlockHash())); exist != nil {
		return nil
	}
	var (
		err       error
		prevBlock *common.Block
	)
	if rootBlockQc := cs.blockPool.GetRootQC(); curLevel <= rootBlockQc.GetLevel() {
		return fmt.Errorf("insertBlock failed, older block, blkLevel: %d, rootLevel: %d", curLevel, rootBlockQc.Level)
	}
	if prevBlock = cs.blockPool.GetBlockByID(string(block.GetHeader().GetPreBlockHash())); prevBlock == nil {
		return fmt.Errorf("insertBlock failed, get previous block is nil")
	}
	if prevBlock.GetHeader().GetBlockHeight()+1 != block.GetHeader().GetBlockHeight() {
		return fmt.Errorf("insertBlock failed, invalid block height [%v], expected [%v]", block.GetHeader().GetBlockHeight(),
			prevBlock.GetHeader().BlockHeight+1)
	}

	if preQc := cs.blockPool.GetQCByID(string(prevBlock.Header.BlockHash)); preQc != nil && preQc.GetLevel() >= curLevel {
		return fmt.Errorf("insertBlock failed, invalid block level, blkLevel: %d, prevQc: %v", curLevel, preQc)
	}
	if err = cs.blockPool.InsertBlock(block); err != nil {
		return fmt.Errorf("insertBlock failed: %s, failed to insert block %v", err, block.GetHeader().GetBlockHeight())
	}
	return nil
}

func (cs *chainStore) commitBlock(block *common.Block) (lastCommitted *common.Block,
	lastCommittedLevel uint64, err error) {
	var (
		blocks []*common.Block
		qc     *maxbftpb.QuorumCert
	)
	if blocks = cs.blockPool.BranchFromRoot(block); len(blocks) == 0 {
		return nil, 0, fmt.Errorf("commit block failed, no block to be committed")
	}
	cs.logger.Infof("commit BranchFromRoot blocks contains [%v:%v]",
		blocks[0].Header.BlockHeight, blocks[len(blocks)-1].Header.BlockHeight)

	for _, blk := range blocks {
		if qc = cs.blockPool.GetQCByID(string(blk.GetHeader().GetBlockHash())); qc == nil {
			return lastCommitted, lastCommittedLevel, fmt.Errorf("commit block failed, get qc for block is nil")
		}

		if err = cs.blockCommitter.AddBlock(blk); err == commonErrors.ErrBlockHadBeenCommited {
			hadCommitBlock, getBlockErr := cs.blockChainStore.GetBlock(blk.GetHeader().GetBlockHeight())
			if getBlockErr != nil {
				cs.logger.Errorf("commit block failed, block had been committed, get block err, %v",
					getBlockErr)
				return lastCommitted, lastCommittedLevel, getBlockErr
			}
			if !bytes.Equal(hadCommitBlock.GetHeader().GetBlockHash(), blk.GetHeader().GetBlockHash()) {
				cs.logger.Errorf("commit block failed, block had been committed, hash unequal: %v",
					hadCommitBlock.GetHeader().GetBlockHash())
				return lastCommitted, lastCommittedLevel, fmt.Errorf(
					"commit block failed, block had been committed, hash unequal")
			}
		} else if err != nil {
			cs.logger.Errorf("commit block failed, add block err, %v", err)
			return lastCommitted, lastCommittedLevel, err
		}
		lastCommitted, lastCommittedLevel = blk, qc.Level
	}
	cs.logger.Debugf("end commit block, lastCommitBlock:[%d:%x], lastCommitLevel: %d",
		lastCommitted.Header.BlockHeight, lastCommitted.Header.BlockHash, lastCommittedLevel)
	return lastCommitted, lastCommittedLevel, nil
}

func (cs *chainStore) pruneBlockStore(nextRootID string) {
	cs.blockPool.PruneBlock(nextRootID)
}

func (cs *chainStore) addQCToBlk(qc *maxbftpb.QuorumCert) {
	blk := cs.blockPool.GetBlockByID(string(qc.BlockId))
	if blk == nil {
		return
	}
	if blk.AdditionalData != nil && len(blk.AdditionalData.ExtraData["QC"]) > 0 {
		return
	}

	qcData, err := proto.Marshal(qc)
	if err != nil {
		cs.logger.Fatalf("marshal qc failed, reason: %s", err)
	}
	if err = utils.AddQCtoBlock(blk, qcData); err != nil {
		cs.logger.Fatalf("add qc to block failed, reason: %s", err)
	}
}

// insertQC Only the QC that has received block data will be stored
func (cs *chainStore) insertQC(qc *maxbftpb.QuorumCert, curEpochId uint64) error {
	if qc == nil {
		return fmt.Errorf("insert qc failed, input nil qc")
	}

	if qc.EpochId != curEpochId {
		// When the generation switches, the QC of the rootBlock is added again,
		// and the rootQC is not consistent with the current generation ID of the node
		if hasQC, err := cs.getQC(string(qc.BlockId), qc.Height); hasQC != nil {
			cs.logger.Warnf("find qc:[%x], height:[%d]", qc.BlockId, qc.Height)
		} else if err != nil {
			cs.logger.Warnf("getQC from chainStore error:%+v", err)
		}
		return fmt.Errorf("insert qc failed, input err qc.epochid: [%v], node epochID: [%v]",
			qc.EpochId, curEpochId)
	}
	if err := cs.blockPool.InsertQC(qc); err != nil {
		return fmt.Errorf("insert qc failed, err: %v", err)
	}
	return nil
}

func (cs *chainStore) insertCompletedBlock(block *common.Block) error {
	if block.GetHeader().GetBlockHeight() <= cs.getCommitHeight() {
		return nil
	}
	if err := cs.updateCommitCacheInfo(block); err != nil {
		return fmt.Errorf("insertCompleteBlock failed, update store commit cache info err %v", err)
	}
	if err := cs.blockPool.InsertBlock(block); err != nil {
		return err
	}
	// todo. may be delete the line
	if err := cs.blockPool.InsertQC(cs.commitQuorumCert); err != nil {
		return err
	}
	cs.pruneBlockStore(string(block.GetHeader().GetBlockHash()))
	return nil
}

func (cs *chainStore) getBlock(id string, height uint64) (*common.Block, error) {
	if block := cs.blockPool.GetBlockByID(id); block != nil {
		return block, nil
	}
	return cs.blockChainStore.GetBlock(height)
}

func (cs *chainStore) getBlockByHash(blkHash []byte) *common.Block {
	if block := cs.blockPool.GetBlockByID(string(blkHash)); block != nil {
		return block
	}
	if block, err := cs.blockChainStore.GetBlockByHash(blkHash); err == nil && block != nil {
		return block
	}
	return nil
}

func (cs *chainStore) getCurrentQC() *maxbftpb.QuorumCert {
	return cs.blockPool.GetHighestQC()
}

func (cs *chainStore) getCurrentCertifiedBlock() *common.Block {
	return cs.blockPool.GetHighestCertifiedBlock()
}

func (cs *chainStore) getRootLevel() (uint64, error) {
	return utils.GetLevelFromQc(cs.blockPool.GetRootBlock())
}

func (cs *chainStore) getQCById(id string) *maxbftpb.QuorumCert {
	return cs.blockPool.GetQCByID(id)
}

func (cs *chainStore) getQC(id string, height uint64) (*maxbftpb.QuorumCert, error) {
	if qc := cs.blockPool.GetQCByID(id); qc != nil {
		return qc, nil
	}
	block, err := cs.blockChainStore.GetBlock(height)
	if err != nil {
		return nil, fmt.Errorf("get qc failed, get block fail at height [%v]", height)
	}
	qcData := utils.GetQCFromBlock(block)
	if qcData == nil {
		return nil, fmt.Errorf("get qc failed, nil qc from block at height [%v]", height)
	}
	qc := new(maxbftpb.QuorumCert)
	if err = proto.Unmarshal(qcData, qc); err != nil {
		return nil, fmt.Errorf("get qc failed, unmarshal qc from a block err %v", err)
	}
	return qc, nil
}
