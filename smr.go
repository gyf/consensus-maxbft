/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"chainmaker.org/chainmaker/consensus-maxbft/v2/liveness"
	safetyrules "chainmaker.org/chainmaker/consensus-maxbft/v2/safety_rules"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/protocol/v2"
)

//maxbftSMR manages current smr of consensus at height and level
type maxbftSMR struct {
	info        *contractInfo            // The governance contract info that be cached
	state       maxbftpb.ConsStateType   // The current consensus state of the local node
	paceMaker   *liveness.Pacemaker      // govern the advancement of levels and height of the local node
	safetyRules *safetyrules.SafetyRules // validate incoming qc and block and update its' state to the newest

	logger protocol.Logger
}

//newMaxBftSMR returns an instance of consensus smr
func newMaxBftSMR(server *ConsensusMaxBftImpl, epoch *epochManager) (*maxbftSMR, error) {
	smr := &maxbftSMR{
		paceMaker: liveness.NewPacemaker(
			server.logger,
			epoch.index,
			epoch.createHeight,
			epoch.createLevel,
			epoch.epochId,
			server.timerService),
		safetyRules: safetyrules.NewSafetyRules(
			server.logger,
			server.chainStore.blockPool,
			server.chainStore.blockChainStore),
		logger: server.logger,
	}
	smr.info = newContractInfo(epoch.governanceContract)
	smr.logger.Debugf("currPeers [%v], lastPeers [%v], switchHeight: %d",
		epoch.governanceContract.Validators, epoch.governanceContract.LastValidators,
		epoch.governanceContract.NextSwitchHeight)
	return smr, nil
}

func (cs *maxbftSMR) updateContractInfo(epoch *epochManager) {
	cs.info = newContractInfo(epoch.governanceContract)
}

func (cs *maxbftSMR) updateState(newState maxbftpb.ConsStateType) {
	cs.state = newState
}

func (cs *maxbftSMR) peers(blkHeight uint64) []*peer {
	return cs.info.getPeers(blkHeight)
}

func (cs *maxbftSMR) getPeerByIndex(blkHeight uint64, index uint64) *peer {
	return cs.info.getPeerByIndex(blkHeight, index)
}

func (cs *maxbftSMR) isValidIdx(index uint64, blkHeight uint64) bool {
	return cs.info.isValidIdx(index, blkHeight)
}

func (cs *maxbftSMR) min(qcHeight uint64) int {
	return cs.info.minQuorumForQc(qcHeight)
}

func (cs *maxbftSMR) getPeers(blkHeight uint64) []*peer {
	return cs.info.getPeers(blkHeight)
}

func (cs *maxbftSMR) getLastVote() (uint64, *maxbftpb.ConsensusPayload) {
	return cs.safetyRules.GetLastVoteLevel(), cs.safetyRules.GetLastVoteMsg()
}

func (cs *maxbftSMR) setLastVote(vote *maxbftpb.ConsensusPayload, level uint64) {
	cs.safetyRules.SetLastVote(vote, level)
}

func (cs *maxbftSMR) safeNode(proposal *maxbftpb.ProposalData) error {
	return cs.safetyRules.SafeNode(proposal)
}

func (cs *maxbftSMR) updateLockedQC(qc *maxbftpb.QuorumCert) {
	cs.safetyRules.UpdateLockedQC(qc)
}

func (cs *maxbftSMR) commitRules(qc *maxbftpb.QuorumCert) (commit bool, commitBlock *common.Block,
	commitLevel uint64) {
	return cs.safetyRules.CommitRules(qc)
}

//func (cs *maxbftSMR) setLastCommittedBlock(block *common.Block, level uint64) {
//	cs.safetyRules.SetLastCommittedBlock(block, level)
//}

func (cs *maxbftSMR) setLastCommittedLevel(level uint64) {
	cs.safetyRules.SetLastCommittedLevel(level)
}

func (cs *maxbftSMR) getLastCommittedLevel() uint64 {
	return cs.safetyRules.GetLastCommittedLevel()
}

func (cs *maxbftSMR) getHeight() uint64 {
	return cs.paceMaker.GetHeight()
}
func (cs *maxbftSMR) getEpochId(blkHeight uint64) uint64 {
	return cs.info.getEpochId(blkHeight)
}

func (cs *maxbftSMR) setEpochId(epochId uint64) {
	cs.paceMaker.SetEpochId(epochId)
}

func (cs *maxbftSMR) getCurrentLevel() uint64 {
	return cs.paceMaker.GetCurrentLevel()
}

func (cs *maxbftSMR) processLocalTimeout(level uint64) bool {
	return cs.paceMaker.ProcessLocalTimeout(level)
}

func (cs *maxbftSMR) getHighestTCLevel() uint64 {
	return cs.paceMaker.GetHighestTCLevel()
}

// processCertificates Update the status of local Pacemaker with the latest received QC;
// height The height of the received block
// hqcLevel The highest QC level in local node;
// htcLevel The received tcLevel
// hcLevel The latest committed QC level in local node.
// Returns true if the consensus goes to the next level, otherwise false.
//func (cs *maxbftSMR) processCertificates(height, hqcLevel, htcLevel, hcLevel uint64) bool {
func (cs *maxbftSMR) processCertificates(qc *maxbftpb.QuorumCert, tc *maxbftpb.QuorumCert,
	hcLevel uint64) bool {
	return cs.paceMaker.ProcessCertificates(qc, tc, hcLevel)
}

func (cs *maxbftSMR) updateTC(tc *maxbftpb.QuorumCert) {
	cs.paceMaker.UpdateTC(tc)
}

func (cs *maxbftSMR) getTC() *maxbftpb.QuorumCert {
	return cs.paceMaker.GetTC()
}
