///*
//Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
//
//SPDX-License-Identifier: Apache-2.0
//*/
package maxbft

import (
	"testing"

	blockpool "chainmaker.org/chainmaker/consensus-maxbft/v2/block_pool"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/liveness"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/utils/v2"

	"github.com/stretchr/testify/require"
)

func TestFetchBlocks(t *testing.T) {
	blkId := utils.GetRandTxId()
	rootQc := &maxbftpb.QuorumCert{BlockId: []byte(blkId)}
	rootBlk := &common.Block{Header: &common.BlockHeader{BlockHash: []byte(blkId)}}
	impl := &ConsensusMaxBftImpl{
		smr: &maxbftSMR{
			paceMaker: &liveness.Pacemaker{},
		},
	}
	impl.chainStore = &chainStore{
		msgbus:    impl.msgbus,
		blockPool: blockpool.NewBlockPool(rootBlk, rootQc),
	}
	blks, qces := generateBlkPair([]byte(blkId), 10)
	for i := 0; i < 10; i++ {
		require.NoError(t, impl.chainStore.insertQC(qces[i], 0))
		require.NoError(t, impl.chainStore.insertBlock(blks[i], uint64(i+1)))
	}
	req := &maxbftpb.BlockFetchMsg{
		Height:      10,
		LockedBLock: []byte(blkId),
		BlockId:     blks[len(blks)-1].Header.BlockHash,
	}
	fetchBlks, status := impl.fetchBlocks(req)
	require.EqualValues(t, maxbftpb.BlockFetchStatus_SUCCEEDED, status)
	for i, blkPair := range fetchBlks {
		require.EqualValues(t, blks[i].Header.BlockHeight, blkPair.Block.Header.BlockHeight)
		require.EqualValues(t, blks[i].Header.BlockHash, blkPair.Block.Header.BlockHash)
		require.EqualValues(t, blks[i].Header.BlockHash, blkPair.Qc.BlockId)
	}
}

func TestNewResponse(t *testing.T) {
	rootBlkId := []byte(utils.GetRandTxId())
	impl := &ConsensusMaxBftImpl{}

	// first exact division
	firstBlkBacth, firstQcBatch := generateBlkPair(rootBlkId, MaxSyncBlockNum*2)
	firstPairs := make([]*maxbftpb.BlockPair, 0, MaxSyncBlockNum*2)
	for i, blk := range firstBlkBacth {
		firstPairs = append(firstPairs, &maxbftpb.BlockPair{Block: blk, Qc: firstQcBatch[i]})
	}
	require.EqualValues(t, 2, len(impl.newResponses(firstPairs, maxbftpb.BlockFetchStatus_SUCCEEDED, 0)))

	// no exact division
	noExactDivBlkBatch, noExactDivBlkQc := generateBlkPair(rootBlkId, MaxSyncBlockNum*2+3)
	noExactDivPairs := make([]*maxbftpb.BlockPair, 0, MaxSyncBlockNum*2+3)
	for i, blk := range noExactDivBlkBatch {
		noExactDivPairs = append(noExactDivPairs, &maxbftpb.BlockPair{Block: blk, Qc: noExactDivBlkQc[i]})
	}
	require.EqualValues(t, 3, len(impl.newResponses(noExactDivPairs, maxbftpb.BlockFetchStatus_SUCCEEDED, 0)))
}

func generateBlkPair(rootHash []byte, num int) ([]*common.Block, []*maxbftpb.QuorumCert) {
	preHash := rootHash
	blks := make([]*common.Block, 0, num)
	qces := make([]*maxbftpb.QuorumCert, 0, num)
	for i := 0; i < num; i++ {
		id := []byte(utils.GetRandTxId())
		blks = append(blks, &common.Block{Header: &common.BlockHeader{
			BlockHash: id, BlockHeight: uint64(i + 1), PreBlockHash: preHash}})
		qces = append(qces, &maxbftpb.QuorumCert{BlockId: id, Height: uint64(i + 1)})
		preHash = id
	}
	return blks, qces
}
