/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"fmt"
	"runtime"
	"strings"
	"sync"

	consensusUtils "chainmaker.org/chainmaker/consensus-utils/v2"
	"chainmaker.org/chainmaker/pb-go/v2/consensus"
	"chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/protocol/v2"
	"github.com/panjf2000/ants/v2"
)

func newVerifySigAlgo(cbi *ConsensusMaxBftImpl) (verifySigAlgo, error) {
	if strings.EqualFold(strings.ToLower(cbi.signAlgoInVote), strings.ToLower(consensusUtils.ECDSAAlgo)) {
		return NewSecpAlgo(cbi), nil
	} else if strings.EqualFold(strings.ToLower(cbi.signAlgoInVote), strings.ToLower(consensusUtils.ThresholdAlgo)) {
		return NewThresholdAlgo(), nil
	}
	return nil, fmt.Errorf("not supported sigAlgoType: %s", cbi.signAlgoInVote)
}

type verifySigAlgo interface {
	verifyVote(vote *maxbft.VoteData) error
	verifyVotes(validThreshold int, votes []*maxbft.VoteData) (reachThreshold bool, err error)
}

type SecpAlgo struct {
	cbi *ConsensusMaxBftImpl
}

func NewSecpAlgo(cbi *ConsensusMaxBftImpl) *SecpAlgo {
	return &SecpAlgo{cbi: cbi}
}

func (s *SecpAlgo) verifyVote(vote *maxbft.VoteData) error {
	if !s.cbi.checkVoteInSingle {
		return nil
	}
	return s.cbi.verifyVote(vote)
}

func (s *SecpAlgo) verifyVotes(validThreshold int, votes []*maxbft.VoteData) (reachThreshold bool, err error) {
	if s.cbi.checkVoteInSingle {
		return len(votes) >= validThreshold, nil
	}
	validators := s.cbi.smr.getPeers(votes[0].Height)
	members := make([]*consensus.GovernanceMember, 0, len(validators))
	for _, v := range validators {
		members = append(members, &consensus.GovernanceMember{NodeId: v.id, Index: v.index})
	}
	return verifyVotes(s.cbi.acp, validThreshold, votes, members, true)
}

func verifyVotes(
	ac protocol.AccessControlProvider,
	validThreshold int,
	votes []*maxbft.VoteData,
	members []*consensus.GovernanceMember,
	delErrVote bool) (reachThreshold bool, err error) {
	verifyPool, err := ants.NewPool(runtime.NumCPU(), ants.WithPreAlloc(true))
	if err != nil {
		return false, fmt.Errorf("create goroutine pool failed, err: %s", err)
	}
	defer verifyPool.Release()

	var (
		wg   = sync.WaitGroup{}
		errs = make([]error, 0, len(votes))
	)
	wg.Add(len(votes))
	for i := range votes {
		index := i
		if err = verifyPool.Submit(func() {
			defer wg.Done()
			if err = validateVoteData(votes[index], members, ac); err != nil {
				errs = append(errs, fmt.Errorf("%d vote validate err: %s", index, err))
			}
		}); err != nil {
			return false, fmt.Errorf("goroutine pool submit task failed, err: %s", err)
		}
	}
	wg.Wait()

	reachThreshold = len(votes)-len(errs) >= validThreshold
	if len(errs) > 0 {
		return reachThreshold, fmt.Errorf(
			"verify votes error, because some vote is wrong; %v", errs)
	}
	return reachThreshold, nil
}

type ThresholdAlgo struct {
}

func NewThresholdAlgo() *ThresholdAlgo {
	return &ThresholdAlgo{}
}

func (t *ThresholdAlgo) verifyVote(vote *maxbft.VoteData) error {
	panic("implement me")
}

func (t *ThresholdAlgo) verifyVotes(validThreshold int, votes []*maxbft.VoteData) (reachThreshold bool, err error) {
	panic("implement me")
}
