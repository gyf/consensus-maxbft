/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package blockpool

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"

	"chainmaker.org/chainmaker/utils/v2"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
)

var (
	rootQc        *maxbftpb.QuorumCert
	blocks        = make([]*common.Block, 20)
	blockPool     *BlockPool
	rootBlockHash = []byte(utils.GetRandTxId())
)

func TestNewBlockPool(t *testing.T) {
	blocks[0] = &common.Block{Header: &common.BlockHeader{BlockHash: rootBlockHash}}
	rootQc = &maxbftpb.QuorumCert{
		BlockId: blocks[0].Header.BlockHash,
	}
	blockPool = NewBlockPool(blocks[0], rootQc)
	require.True(t, blockPool.idToQC[string(rootQc.BlockId)] == rootQc)
}

func TestInsertBlock(t *testing.T) {
	blocks[1] = &common.Block{Header: &common.BlockHeader{
		BlockHash:    []byte(utils.GetRandTxId()),
		PreBlockHash: rootBlockHash,
		BlockHeight:  1}}
	err := blockPool.InsertBlock(blocks[1])
	fmt.Println(blockPool.idToQC[string(blocks[1].Header.BlockHash)])
	fmt.Println(blocks[0].Header.BlockHeight)
	fmt.Println(blocks[1].Header.BlockHeight)
	fmt.Println(blockPool.highestCertifiedBlock.Header.BlockHeight)
	require.Nil(t, err)
}
