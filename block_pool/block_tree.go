/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package blockpool

import (
	"bytes"
	"errors"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"

	"chainmaker.org/chainmaker/common/v2/queue"
	"chainmaker.org/chainmaker/pb-go/v2/common"
)

//BlockNode save one block and its children
type BlockNode struct {
	block    *common.Block
	children []string // the blockHash with children's block
}

//GetBlock get block
func (bn *BlockNode) GetBlock() *common.Block {
	return bn.block
}

//GetChildren get children
func (bn *BlockNode) GetChildren() []string {
	return bn.children
}

//BlockTree maintains a consistent block tree of parent and children links
//this struct is not thread safety.
type BlockTree struct {
	idToNode       map[string]*BlockNode // store block and its' children blockHash
	heightToBlocks map[uint64][]*common.Block
	rootBlock      *common.Block      // The first block is committed to the chain
	rootQC         *maxbft.QuorumCert // the first block has been committed to the chain
}

//NewBlockTree init a block tree with rootBlock, rootQC and maxPrunedSize
func NewBlockTree(rootBlock *common.Block, rootQC *maxbft.QuorumCert) *BlockTree {
	blockTree := &BlockTree{
		idToNode:       make(map[string]*BlockNode, 10),
		rootQC:         rootQC,
		rootBlock:      rootBlock,
		heightToBlocks: make(map[uint64][]*common.Block),
	}
	blockTree.idToNode[string(rootBlock.Header.BlockHash)] = &BlockNode{
		block:    rootBlock,
		children: make([]string, 0),
	}
	blockTree.heightToBlocks[rootBlock.Header.BlockHeight] = append(
		blockTree.heightToBlocks[rootBlock.Header.BlockHeight], rootBlock)
	return blockTree
}

//InsertBlock insert block to tree
func (bt *BlockTree) InsertBlock(block *common.Block) error {
	if block == nil {
		return errors.New("block is nil")
	}
	if _, exist := bt.idToNode[string(block.Header.BlockHash)]; exist {
		return nil
	}
	if _, exist := bt.idToNode[string(block.Header.PreBlockHash)]; !exist {
		return errors.New("block's parent not exist")
	}

	bt.idToNode[string(block.Header.BlockHash)] = &BlockNode{
		block:    block,
		children: make([]string, 0),
	}
	preBlock := bt.idToNode[string(block.Header.PreBlockHash)]
	preBlock.children = append(preBlock.children, string(block.Header.BlockHash))
	bt.heightToBlocks[block.Header.BlockHeight] = append(bt.heightToBlocks[block.Header.BlockHeight], block)
	return nil
}

//GetRootBlock get root block from tree
func (bt *BlockTree) GetRootBlock() *common.Block {
	return bt.rootBlock
}

func (bt *BlockTree) GetRootQC() *maxbft.QuorumCert {
	return bt.rootQC
}

//GetBlockByID get block by block hash
func (bt *BlockTree) GetBlockByID(id string) *common.Block {
	if node, ok := bt.idToNode[id]; ok {
		return node.GetBlock()
	}
	return nil
}

//BranchFromRoot get branch from root to input block
func (bt *BlockTree) BranchFromRoot(block *common.Block) []*common.Block {
	if block == nil {
		return nil
	}
	var (
		cur    = block
		branch []*common.Block
	)
	//use block height to check
	for cur.Header.BlockHeight > bt.rootBlock.Header.BlockHeight {
		branch = append(branch, cur)
		if cur = bt.GetBlockByID(string(cur.Header.PreBlockHash)); cur == nil {
			break
		}
	}

	if cur == nil || !bytes.Equal(cur.Header.BlockHash, bt.rootBlock.Header.BlockHash) {
		return nil
	}
	for i, j := 0, len(branch)-1; i < j; i, j = i+1, j-1 {
		branch[i], branch[j] = branch[j], branch[i]
	}
	return branch
}

//PruneBlock prune block and update rootBlock
func (bt *BlockTree) PruneBlock(newRootId string, newRootQC *maxbft.QuorumCert) []string {
	toPruned := bt.findBlockToPrune(newRootId)
	if toPruned == nil {
		return nil
	}
	newRootBlock := bt.GetBlockByID(newRootId)
	if newRootBlock == nil {
		return nil
	}
	bt.rootBlock = newRootBlock
	bt.rootQC = newRootQC

	var pruned []string
	num := len(toPruned)
	for i := 0; i < num; i++ {
		bt.cleanBlock(toPruned[i])
		pruned = append(pruned, toPruned[i])
	}
	return pruned
}

//findBlockToPrune get blocks to prune by the newRootID
func (bt *BlockTree) findBlockToPrune(newRootId string) []string {
	if newRootId == "" || newRootId == string(bt.rootBlock.Header.BlockHash) {
		return nil
	}
	var (
		toPruned      []string
		toPrunedQueue = queue.NewLinkedQueue()
	)
	toPrunedQueue.PushBack(string(bt.rootBlock.Header.BlockHash))
	for !toPrunedQueue.IsEmpty() {
		curId, _ := toPrunedQueue.PollFront().(string)
		curNode := bt.idToNode[curId]
		for _, child := range curNode.GetChildren() {
			if child == newRootId {
				continue //save this branch
			}
			toPrunedQueue.PushBack(child)
		}
		toPruned = append(toPruned, curId)
	}
	return toPruned
}

//cleanBlock remove block from tree
func (bt *BlockTree) cleanBlock(blockId string) {
	blk := bt.idToNode[blockId]
	delete(bt.idToNode, blockId)
	if blk != nil {
		delete(bt.heightToBlocks, blk.block.Header.BlockHeight)
	}
}

func (bt *BlockTree) GetBlocks(height uint64) []*common.Block {
	return bt.heightToBlocks[height]
}

func (bt *BlockTree) Details() string {
	blkContents := bytes.NewBufferString(fmt.Sprintf("BlockTree blockNum: %d\n", len(bt.idToNode)))
	for _, blks := range bt.heightToBlocks {
		for _, blk := range blks {
			blkContents.WriteString(fmt.Sprintf("blkID: %x, blockHeight:%d\n", blk.Header.BlockHash, blk.Header.BlockHeight))
		}
	}
	return blkContents.String()
}
