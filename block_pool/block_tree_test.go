/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package blockpool

import (
	"testing"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/utils/v2"

	"github.com/gogo/protobuf/proto"
)

func TestBlockTree_ProcessBlock(t *testing.T) {
	var (
		block  *common.Block
		qc     *maxbftpb.QuorumCert
		rootQc *maxbftpb.QuorumCert
		blk    *common.Block
		blks   []*common.Block
		blocks = make([]*common.Block, 20)
	)
	blocks[0] = &common.Block{Header: &common.BlockHeader{BlockHash: []byte(utils.GetRandTxId())}}
	rootQc = &maxbftpb.QuorumCert{
		BlockId: blocks[0].Header.BlockHash,
	}
	tree := NewBlockTree(blocks[0], rootQc)
	for i := 1; i < 20; i++ {
		block = &common.Block{
			Header: &common.BlockHeader{
				BlockHash:    []byte(utils.GetRandTxId()),
				BlockHeight:  uint64(i),
				PreBlockHash: blocks[i-1].Header.BlockHash,
			},
		}
		_ = tree.InsertBlock(block)
		blocks[i] = block
	}
	qc = tree.GetRootQC()
	if !proto.Equal(qc, rootQc) {
		t.Errorf("GetRootQC error. qc:%+v", qc)
	}
	blk = tree.GetRootBlock()
	if !proto.Equal(blk, blocks[0]) {
		t.Errorf("GetRootBlock error. block:%+v", blk)
	}
	blks = tree.GetBlocks(uint64(5))
	if len(blks) != 1 || !proto.Equal(blks[0], blocks[5]) {
		t.Errorf("GetBlocks error. block:%+v, want:%+v", blks, blocks[5])
	}
	blk = tree.GetBlockByID(string(blocks[7].Header.BlockHash))
	if !proto.Equal(blk, blocks[7]) {
		t.Errorf("GetBlockByID error. block:%+v", blk)
	}
	blks = tree.BranchFromRoot(blocks[8])
	if len(blks) != 8 {
		t.Errorf("BranchFromRoot error. length:%d", len(blks))
	}
	for i := 0; i < 8; i++ {
		if !proto.Equal(blks[i], blocks[i+1]) {
			t.Errorf("BranchFromRoot error. block:%+v, want:%+v", blks[i], blocks[i])
		}
	}
	rootQc = &maxbftpb.QuorumCert{
		BlockId: blocks[15].Header.BlockHash,
	}
	blkIds := tree.PruneBlock(string(blocks[15].Header.BlockHash), rootQc)
	if len(blkIds) != 15 {
		t.Errorf("PruneBlock error. length:%d, \nblkIds:%+v, \nblocks:%+v", len(blkIds), blkIds, blocks)
	}
	blk = tree.GetRootBlock()
	if !proto.Equal(blk, blocks[15]) {
		t.Errorf("PruneBlock error. root block:%+v", blk)
	}
	qc = tree.GetRootQC()
	if !proto.Equal(qc, rootQc) {
		t.Errorf("PruneBlock error. root qc:%+v", qc)
	}
	blkIds = tree.PruneBlock(string(blocks[18].Header.BlockHash), rootQc)
	if len(blkIds) != 3 {
		t.Errorf("PruneBlock error. length:%d, \nblkIds:%+v, \nblocks:%+v", len(blkIds), blkIds, blocks)
	}
	blk = tree.GetRootBlock()
	if !proto.Equal(blk, blocks[18]) {
		t.Errorf("PruneBlock error. root block:%+v", blk)
	}
	qc = tree.GetRootQC()
	if !proto.Equal(qc, rootQc) {
		t.Errorf("PruneBlock error. root qc:%+v", qc)
	}

	details := tree.Details()
	if details == "" {
		t.Errorf("GetDetails error. details Empty")
	}
}
