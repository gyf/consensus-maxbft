/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"sort"

	commonErrors "chainmaker.org/chainmaker/common/v2/errors"
	"chainmaker.org/chainmaker/common/v2/msgbus"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/governance"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/utils"
	timeservice "chainmaker.org/chainmaker/consensus-utils/v2/time_service"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/consensus"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/protocol/v2"
	chainUtils "chainmaker.org/chainmaker/utils/v2"
	"github.com/gogo/protobuf/proto"
)

var (
	ErrInvalidPeer  = errors.New("invalid peer")
	ErrValidateSign = errors.New("validate sign error")
)

//processNewHeight If the local node is one of the validators in current epoch,
//update SMR state to ConsStateType_NEW_LEVEL and prepare to generate a new block
//if local node is proposer in the current level
func (cbi *ConsensusMaxBftImpl) processNewHeight(height uint64, level uint64) {
	if cbi.smr.state != maxbftpb.ConsStateType_NEW_HEIGHT {
		cbi.logger.Debugf("service selfIndexInEpoch [%v] processNewHeight: "+
			"height [%v] level [%v] state %v", cbi.selfIndexInEpoch, height, level, cbi.smr.state.String())
		return
	}
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processNewHeight at "+
		"height [%v] level [%v],  state %v epoch %v",
		cbi.selfIndexInEpoch, height, level, cbi.smr.state, cbi.smr.paceMaker.GetEpochId())
	if !cbi.smr.isValidIdx(cbi.selfIndexInEpoch, height) {
		cbi.logger.Infof("self selfIndexInEpoch [%v] is not in current consensus epoch", cbi.selfIndexInEpoch)
		return
	}
	cbi.smr.updateState(maxbftpb.ConsStateType_NEW_LEVEL)
	cbi.processNewLevel(height, level)
}

//processNewLevel update state to ConsStateType_PROPOSE and prepare to generate
//a new block if local node is proposer in the current level
func (cbi *ConsensusMaxBftImpl) processNewLevel(height uint64, level uint64) {
	if cbi.smr.getHeight() != height || cbi.smr.getCurrentLevel() > level ||
		cbi.smr.state != maxbftpb.ConsStateType_NEW_LEVEL {
		cbi.logger.Debugf("service selfIndexInEpoch [%v] processNewLevel: "+
			"invalid input [%v:%v], smr height [%v] level [%v] state %v", cbi.selfIndexInEpoch,
			height, level, cbi.smr.getHeight(), cbi.smr.getCurrentLevel(), cbi.smr.state)
		return
	}

	cbi.logger.Debugf("service selfIndexInEpoch [%v] processNewLevel at height [%v]"+
		" level [%v], smr height [%v] level [%v] state %v", cbi.selfIndexInEpoch, height, level,
		cbi.smr.getHeight(), cbi.smr.getCurrentLevel(), cbi.smr.state)
	hqcBlock := cbi.chainStore.getCurrentCertifiedBlock()
	hqcLevel, err := utils.GetLevelFromBlock(hqcBlock)
	if err != nil {
		cbi.logger.Errorf("get level from block failed, error %v, height [%v]", err, hqcBlock.Header.BlockHeight)
		return
	}
	if hqcLevel >= level {
		cbi.logger.Errorf("given level [%v] too low than certified %v", level, hqcLevel)
		return
	}
}

func (cbi *ConsensusMaxBftImpl) processNewPropose(height, level uint64, preBlkHash []byte) {
	cbi.logger.Debugf("begin processNewPropose block:[%d:%d], preHash:%x, "+
		"nodeStatus: %s", height, level, preBlkHash, cbi.smr.state.String())
	nextProposerIndex := cbi.getProposer(height, level)
	if cbi.isValidProposer(height, level, cbi.selfIndexInEpoch) {
		cbi.logger.Infof("service selfIndexInEpoch [%v], build proposal, height: [%v], level [%v]",
			cbi.selfIndexInEpoch, height, level)
		cbi.msgbus.Publish(msgbus.BuildProposal, &maxbftpb.BuildProposal{
			Height:     height,
			IsProposer: true,
			PreHash:    preBlkHash,
		})
		cbi.addTimerEvent(&timeservice.TimerEvent{
			Level: level, State: maxbftpb.ConsStateType_PROPOSE, Index: cbi.selfIndexInEpoch,
			Height: height, EpochId: cbi.smr.paceMaker.GetEpochId(),
			Duration: timeservice.GetEventTimeout(timeservice.PROPOSAL_BLOCK_TIMEOUT, 0),
		})
	}
	cbi.logger.Infof("service selfIndexInEpoch [%v], waiting proposal, "+
		"height: [%v], level [%v], nextProposerIndex [%d]", cbi.selfIndexInEpoch, height, level, nextProposerIndex)
}

//processProposedBlock receive proposed block form core module, then go to new level
func (cbi *ConsensusMaxBftImpl) processProposedBlock(block *common.Block) {
	if block == nil {
		cbi.logger.Warnf("received invalid proposed block from core")
		return
	}
	var (
		height  = cbi.smr.getHeight()
		level   = cbi.smr.getCurrentLevel()
		bHeight = block.Header.BlockHeight
	)

	if height != bHeight {
		cbi.logger.Warnf("service id [%v] selfIndexInEpoch [%v] receive proposed block "+
			"height [%v] not equal to smr.height [%v]", cbi.id, cbi.selfIndexInEpoch, block.Header.BlockHeight, height)
		return
	}
	if !cbi.isValidProposer(height, level, cbi.selfIndexInEpoch) {
		return
	}
	cbi.logger.Debugf("processProposedBlock start, block height: [%v], level: [%v]", block.Header.BlockHeight, level)
	blkHash := string(block.Header.BlockHash)
	if len(blkHash) > 0 {
		if data, ok := cbi.createProposals[height][blkHash]; ok {
			cbi.logger.DebugDynamic(func() string {
				return fmt.Sprintf("repeat broadcast same proposal: [%d:%x]", height, blkHash)
			})
			cbi.Broadcast(height, data)
			return
		}
	}

	beginConstruct := chainUtils.CurrentTimeMillisSeconds()
	proposal := cbi.constructProposal(block, level, cbi.smr.getEpochId(bHeight))
	if proposal == nil {
		cbi.logger.Warnf("generate proposal[%d,preHash:%x] failed",
			block.Header.BlockHeight, block.Header.PreBlockHash)
		return
	}
	endConstruct := chainUtils.CurrentTimeMillisSeconds()
	consensusData, err := cbi.signAndMarshal(proposal, true)
	if err != nil {
		cbi.logger.Errorf("sign payload failed, err %v", err)
		return
	}
	if _, exist := cbi.createProposals[height]; !exist {
		cbi.createProposals[height] = make(map[string][]byte)
	}
	cbi.createProposals[height][blkHash] = consensusData
	cbi.Broadcast(height, consensusData)
	endSignAndBroad := chainUtils.CurrentTimeMillisSeconds()
	cbi.logger.Debugf("time used in processProposedBlock, constructProposalTime: %d, "+
		"signAndBroadTime: %d", endConstruct-beginConstruct, endSignAndBroad-endConstruct)
}

func (cbi *ConsensusMaxBftImpl) processProposeTimeout(height, level uint64) {
	if cbi.smr.getHeight() != height || cbi.smr.getCurrentLevel() > level {
		cbi.logger.Debugf("invalid state [%v:%v], smr height [%v] "+
			"level [%v] state %v", height, level, cbi.smr.getHeight(), cbi.smr.getCurrentLevel(), cbi.smr.state)
		return
	}

	highQc := cbi.chainStore.getCurrentQC()
	highBlk := cbi.chainStore.getCurrentCertifiedBlock()
	if !bytes.Equal(highBlk.Header.BlockHash, highQc.BlockId) {
		return
	}
	if highQc.Level >= level {
		return
	}
	cbi.processNewPropose(height, level, highQc.BlockId)
}

func (cbi *ConsensusMaxBftImpl) processLocalTimeout(height uint64, level uint64) {
	if !cbi.smr.processLocalTimeout(level) {
		return
	}
	var (
		err  error
		vote *maxbftpb.ConsensusPayload
	)
	if lastVotedLevel, lastVote := cbi.smr.getLastVote(); lastVotedLevel == level &&
		lastVote.GetVoteMsg().VoteData.Height == height {
		// retry send last vote
		vote, err = cbi.retryVote(lastVote)
	} else {
		vote, err = cbi.constructVote(height, level, cbi.smr.getEpochId(height), nil)
	}
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processLocalTimeout: broadcasts timeout "+
		"vote [%v:%v] to other validators", cbi.selfIndexInEpoch, height, level)
	if err != nil {
		cbi.logger.Errorf("processLocalTimeout get vote error: %s", err)
		return
	}
	cbi.smr.setLastVote(vote, level)
	consensusData, err := cbi.signAndMarshal(vote, true)
	if err != nil {
		cbi.logger.Warnf("create vote msg failed: [%s]", vote.String())
		return
	}
	cbi.Broadcast(height, consensusData)
}

func (cbi *ConsensusMaxBftImpl) retryVote(
	lastVote *maxbftpb.ConsensusPayload) (*maxbftpb.ConsensusPayload, error) {
	var (
		err      error
		data     []byte
		sign     []byte
		voteMsg  = lastVote.GetVoteMsg()
		voteData = voteMsg.VoteData
	)
	cbi.logger.Debugf("service index [%v] processLocalTimeout: "+
		"get last vote [%v:%v] to other validators, BlockId [%x]",
		cbi.selfIndexInEpoch, voteData.Height, voteData.Level, voteData.BlockId)
	// when a node timeouts at the same consensus level, it needs to
	// change the vote type for the current level to a timeout.
	tempVoteData := &maxbftpb.VoteData{
		NewView:   true,
		Level:     voteData.Level,
		Author:    voteData.Author,
		Height:    voteData.Height,
		BlockId:   voteData.BlockId,
		EpochId:   cbi.smr.getEpochId(voteData.Height),
		AuthorIdx: voteData.AuthorIdx,
	}
	if data, err = proto.Marshal(tempVoteData); err != nil {
		return nil, fmt.Errorf("marshal vote failed: %s", err)
	}
	if sign, err = cbi.singer.Sign(cbi.chainConf.ChainConfig().Crypto.Hash, data); err != nil {
		return nil, fmt.Errorf("failed to sign data failed, err %v data %v", err, data)
	}
	//serializeMember, err := cbi.singer.GetSerializedMember(true)
	serializeMember, err := cbi.singer.GetMember()
	if err != nil {
		return nil, fmt.Errorf("failed to get signer serializeMember failed, err %v", err)
	}

	tempVoteData.Signature = &common.EndorsementEntry{
		Signer:    serializeMember,
		Signature: sign,
	}
	return &maxbftpb.ConsensusPayload{
		Type: maxbftpb.MessageType_VOTE_MESSAGE,
		Data: &maxbftpb.ConsensusPayload_VoteMsg{
			VoteMsg: &maxbftpb.VoteMsg{
				VoteData: tempVoteData,
				SyncInfo: voteMsg.SyncInfo,
			}},
	}, nil
}

func (cbi *ConsensusMaxBftImpl) verifyJustifyQC(qc *maxbftpb.QuorumCert) error {
	if !qc.NewView && qc.BlockId == nil {
		return fmt.Errorf("nil block id in qc")
	}
	if qc.NewView && len(qc.BlockId) > 0 {
		return fmt.Errorf("should not have block id:%x in tc", qc.BlockId)
	}
	if qc.Height == 0 {
		return nil
	}
	return cbi.checkVotesInQc(qc)
}

func (cbi *ConsensusMaxBftImpl) checkVotesInQc(qc *maxbftpb.QuorumCert) error {
	newViewNum, votedBlockNum, err := cbi.countNumFromVotes(qc)
	if err != nil {
		return err
	}
	quorum := cbi.smr.min(qc.Height)
	if qc.Level > 0 && qc.NewView && newViewNum < quorum {
		return fmt.Errorf(fmt.Sprintf("vote new view num [%v] less than expected [%v]",
			newViewNum, quorum))
	}
	if qc.Level > 0 && !qc.NewView && votedBlockNum < quorum {
		return fmt.Errorf(fmt.Sprintf("vote block num [%v] less than expected [%v]",
			votedBlockNum, quorum))
	}
	return nil
}

func (cbi *ConsensusMaxBftImpl) needFetch(syncInfo *maxbftpb.SyncInfo) (bool, error) {
	var (
		err       error
		rootLevel uint64
		qc        = syncInfo.HighestQc
	)
	if qc.Height == 0 || len(qc.BlockId) == 0 {
		return false, nil
	}
	if rootLevel, err = cbi.chainStore.getRootLevel(); err != nil {
		return false, fmt.Errorf("get root level fail")
	}
	if qc.Level < rootLevel {
		return false, fmt.Errorf("sync info has a highest quorum certificate with level older than root level")
	}
	if qc.Height > cbi.smr.getHeight()+MaxSyncBlockNum {
		return false, fmt.Errorf("receive data info from future. qc.Height:%d, smrHeight:%d",
			qc.Height, cbi.smr.getHeight())
	}
	hasQCBlk, _ := cbi.chainStore.getBlock(string(qc.BlockId), qc.Height)
	if hasQCBlk == nil {
		cbi.logger.Debugf("service selfIndexInEpoch [%v] needFetch: local not have block [%v:%v:%x]",
			cbi.selfIndexInEpoch, qc.Height, qc.Level, qc.BlockId)
		return true, nil
	}
	hasPreQC, _ := cbi.chainStore.getQC(string(hasQCBlk.Header.PreBlockHash), qc.Height-1)
	if hasPreQC == nil {
		cbi.logger.Debugf("service selfIndexInEpoch [%v] needFetch: local not have preQC [%v:%x]",
			cbi.selfIndexInEpoch, qc.Height-1, hasQCBlk.Header.PreBlockHash)
		return true, nil
	}
	return false, nil
}

func (cbi *ConsensusMaxBftImpl) validateProposalMsg(msg *maxbftpb.ConsensusMsg) error {
	proposal := msg.Payload.GetProposalMsg().ProposalData
	if proposal.Level < cbi.smr.getCurrentLevel() {
		return fmt.Errorf("old proposal, ignore it. proposalInfo:[%d:%d], smrInfo:[%d:%d]",
			proposal.Height, proposal.Level, cbi.smr.getHeight(), cbi.smr.getCurrentLevel())
	}
	if proposal.EpochId != cbi.smr.getEpochId(proposal.Height) {
		return fmt.Errorf("err epochId, ignore it, proposal"+
			" is %d, local node is %d", proposal.EpochId, cbi.smr.getEpochId(proposal.Height))
	}
	if hasMsg := cbi.msgPool.GetProposal(proposal.Height, proposal.Level); hasMsg != nil {
		return fmt.Errorf("specify consensus"+
			" rounds that already have proposals, has proposal hash: %x, "+
			"height: %d, level: %d", proposal.Block.Header.BlockHash, proposal.Height, proposal.Level)
	}
	if err := cbi.validateProposer(msg); err != nil {
		return err
	}

	if err := cbi.verifyJustifyQC(proposal.JustifyQc); err != nil {
		return fmt.Errorf("block [%v:%v verifyJustifyQC failed, err %s", proposal.Height, proposal.Level, err)
	}
	if !bytes.Equal(proposal.JustifyQc.BlockId, proposal.Block.GetHeader().PreBlockHash) {
		cbi.logger.Errorf("service selfIndexInEpoch [%v] validateProposal: mismatch pre hash [%x] in block, "+
			"justifyQC %x", cbi.selfIndexInEpoch, proposal.Block.GetHeader().PreBlockHash, proposal.JustifyQc.BlockId)
		return fmt.Errorf("mismatch pre hash in block header and justify qc")
	}
	if err := cbi.smr.safeNode(proposal); err != nil {
		return fmt.Errorf("verify proposal [%v:%v:%x] by satety rules failed: %s",
			proposal.Height, proposal.Level, proposal.Block.Header.BlockHash, err)
	}
	cbi.logger.Debugf("validate proposal msg success [%d:%d:%d]",
		proposal.ProposerIdx, proposal.Height, proposal.Level)
	return nil
}

func (cbi *ConsensusMaxBftImpl) validateProposer(msg *maxbftpb.ConsensusMsg) error {
	proposal := msg.Payload.GetProposalMsg().ProposalData
	if !cbi.isValidProposer(proposal.Height, proposal.Level, proposal.ProposerIdx) {
		return fmt.Errorf("received a proposal "+
			"at height [%v] level [%v] from invalid selfIndexInEpoch [%v] addr [%v]",
			proposal.Height, proposal.Level, proposal.ProposerIdx, proposal.Proposer)
	}
	err := cbi.validateSignature(msg, cbi.smr.getPeerByIndex(proposal.Height, proposal.ProposerIdx))
	return err
}

func (cbi *ConsensusMaxBftImpl) processFetchResp(msg *maxbftpb.ConsensusMsg) {
	if err := cbi.validateBlockFetchRsp(msg); err != nil {
		return
	}
	payload := msg.Payload.GetBlockFetchRespMsg()
	haveProcessBlocks := make(map[string]bool, len(payload.Blocks))

	sort.Slice(payload.Blocks, func(i, j int) bool {
		return payload.Blocks[i].Block.Header.BlockHeight < payload.Blocks[j].Block.Header.BlockHeight
	})
	for _, pair := range payload.Blocks {
		if err := cbi.validateBlockPair(payload.AuthorIdx, pair, haveProcessBlocks); err != nil {
			cbi.logger.Warnf("%s", err)
			continue
		}
		cbi.logger.Debugf("validate blockPair success: [%d:%d:%x]", pair.Qc.Height, pair.Qc.Level, pair.Qc.BlockId)
		if err := cbi.processBlockAndQC(pair); err != nil {
			cbi.logger.Warnf("%s", err)
		}
	}
}

func (cbi *ConsensusMaxBftImpl) validateBlockPair(fromPeer uint64,
	blockPair *maxbftpb.BlockPair, haveProcessBlocks map[string]bool) error {

	qc := blockPair.Qc
	blkHeader := blockPair.Block.GetHeader()
	if qc.Level < cbi.smr.getCurrentLevel() {
		return fmt.Errorf("old proposal, ignore it. proposalInfo:[%d:%d], smrInfo:[%d:%d]",
			qc.Height, qc.Level, cbi.smr.getHeight(), cbi.smr.getCurrentLevel())
	}
	if qc.EpochId != cbi.smr.getEpochId(qc.Height) {
		return fmt.Errorf("server selfIndexInEpoch [%v] invalid epochID in pairInfo, "+
			"qc epochId %v,expected %v ", cbi.selfIndexInEpoch, qc.EpochId, cbi.smr.getEpochId(qc.Height))
	}
	blockId := string(blkHeader.GetBlockHash())
	if exist := haveProcessBlocks[(blockId)]; exist {
		return fmt.Errorf("duplicated block [%v:%v] id [%x] has been processed", qc.Height, qc.Level, blockId)
	}
	preQC, err := cbi.chainStore.getQC(string(blkHeader.PreBlockHash), qc.Height-1)
	if err != nil {
		return fmt.Errorf("not find preBlockQC, preHeigh[%d]", qc.Height-1)
	}
	if !bytes.Equal(preQC.BlockId, blkHeader.PreBlockHash) {
		return fmt.Errorf("preBlock id[%x] not equal preQC id[%x]", blkHeader.PreBlockHash, qc.BlockId)
	}
	if err := cbi.verifyJustifyQC(qc); err != nil {
		return fmt.Errorf("server selfIndexInEpoch [%v] validate qc "+
			"[%v:%v] failed, err %v", cbi.selfIndexInEpoch, qc.Height, qc.Level, err)
	}
	if qc.Height != uint64(blkHeader.GetBlockHeight()) {
		return fmt.Errorf("server selfIndexInEpoch [%v] mismatch block "+
			"height [%v], expected [%v]", cbi.selfIndexInEpoch, blkHeader.GetBlockHeight(), qc.Height)
	}
	if !bytes.Equal(blkHeader.GetBlockHash(), qc.BlockId) {
		return fmt.Errorf("server selfIndexInEpoch [%v] mismatch block "+
			"id [%x], expected [%x]", cbi.selfIndexInEpoch, qc.BlockId, blkHeader.GetBlockHash())
	}

	if err := cbi.smr.safeNode(&maxbftpb.ProposalData{
		Block: blockPair.Block, JustifyQc: preQC, Level: qc.Level, Height: blockPair.Qc.Height,
	}); err != nil {
		return fmt.Errorf("verify blockPair [%v:%v:%x] by satety rules failed: %s",
			qc.Height, qc.Level, qc.BlockId, err)
	}
	haveProcessBlocks[blockId] = true
	if err := cbi.blockVerifier.VerifyBlock(blockPair.Block,
		protocol.CONSENSUS_VERIFY); err == commonErrors.ErrBlockHadBeenCommited {
		hadCommitBlock, getBlockErr := cbi.store.GetBlock(blkHeader.GetBlockHeight())
		if getBlockErr != nil {
			return fmt.Errorf("service selfIndexInEpoch [%v] VerifyBlock, block had been committed, "+
				"get block err, %v", cbi.selfIndexInEpoch, getBlockErr)
		}
		if !bytes.Equal(hadCommitBlock.GetHeader().GetBlockHash(), blkHeader.GetBlockHash()) {
			return fmt.Errorf("service selfIndexInEpoch [%v] VerifyBlock, commit block failed,"+
				" block had been committed, hash unequal err, %v", cbi.selfIndexInEpoch, getBlockErr)
		}
	} else if err != nil {
		return fmt.Errorf("service selfIndexInEpoch [%v] VerifyBlock failed: invalid block "+
			"from peer [%v] at baseInfo[%d:%d] err %v",
			cbi.selfIndexInEpoch, fromPeer, blkHeader.BlockHeight, qc.Level, err)
	}
	return cbi.validateBlockConsensusArg(blockPair.Block, qc.Level)
}

func (cbi *ConsensusMaxBftImpl) processBlockAndQC(blockPair *maxbftpb.BlockPair) error {
	header := blockPair.Block.GetHeader()
	if executorErr := cbi.chainStore.insertBlock(blockPair.Block, blockPair.Qc.Level); executorErr != nil {
		return fmt.Errorf("insertBlock [%v:%x] failed, err %v",
			header.GetBlockHeight(), header.BlockHash, executorErr)
	}
	err := cbi.processQC(blockPair.Qc)
	return err
}

func (cbi *ConsensusMaxBftImpl) processProposal(msg *maxbftpb.ConsensusMsg) {
	var (
		proposalMsg = msg.Payload.GetProposalMsg()
		proposal    = proposalMsg.ProposalData
	)
	st := chainUtils.CurrentTimeMillisSeconds()
	cbi.logger.Infof("service selfIndexInEpoch [%v] processProposal step0. "+
		"proposal.ProposerIdx [%v] ,proposal.Height[%v], proposal.Level[%v],"+
		"proposal.EpochId [%v],expected [%v:%v:%v]", cbi.selfIndexInEpoch, proposal.ProposerIdx,
		proposal.Height, proposal.Level, proposal.EpochId, cbi.smr.getHeight(),
		cbi.smr.getCurrentLevel(), cbi.smr.getEpochId(proposal.Height))

	//step0: validate proposal
	if err := cbi.validateProposalMsg(msg); err != nil {
		cbi.logger.Errorf("service selfIndexInEpoch [%v] processProposal validate proposal failed, err %v",
			cbi.selfIndexInEpoch, err)
		return
	}
	cbi.logger.Debugf("validate proposal msg success [%d:%d:%d]",
		proposal.ProposerIdx, proposal.Height, proposal.Level)

	//step1: fetch data, not received fetch
	if needFetch, err := cbi.fetchDataIfRequire(proposalMsg); err != nil {
		cbi.logger.Errorf("service selfIndexInEpoch [%v] processProposal fetch data failed, err %v",
			cbi.selfIndexInEpoch, err)
		return
	} else if needFetch {
		cbi.logger.Warnf("need fetch miss data from peer by proposal:[%d:%d:%x]",
			proposal.Height, proposal.Level, proposal.Block.Header.BlockHash)
		return
	}
	//step2: validate new block from proposal
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal step2 validate "+
		"new block from proposal start", cbi.selfIndexInEpoch)
	if err := cbi.validateBlock(proposal); err != nil {
		cbi.logger.Errorf("%s", err)
		return
	}
	//step3: validate consensus args
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal step3 validate consensus args",
		cbi.selfIndexInEpoch)
	if err := cbi.validateBlockConsensusArg(proposal.Block, proposal.Level); err != nil {
		cbi.logger.Errorf("%s", err)
		return
	}
	//step4: vote the proposal
	cbi.voteForProposal(proposal)
	cbi.smr.updateState(maxbftpb.ConsStateType_PROPOSE)
	cbi.logger.Infof("[processProposal]start to processNewPropose, proposal.Height:%+v, leve:%+v, blcokHash:%+v",
		proposal.Height+1, proposal.Level+1, proposal.Block.Header.BlockHash)
	cbi.processNewPropose(proposal.Height+1, proposal.Level+1, proposal.Block.Header.BlockHash)
	cbi.processProposalPublished(msg)
	cbi.logger.Debugf("processProposal time used: %d", chainUtils.CurrentTimeMillisSeconds()-st)
}

func (cbi *ConsensusMaxBftImpl) processProposalPublished(msg *maxbftpb.ConsensusMsg) {
	var (
		proposalMsg = msg.Payload.GetProposalMsg()
		proposal    = proposalMsg.ProposalData
	)
	//step4: process the qc from proposal
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal step3 process qc start", cbi.selfIndexInEpoch)
	if cbi.doneReplayWal {
		cbi.addProposalWalIndex(proposal.Height)
		cbi.saveWalEntry(maxbftpb.MessageType_PROPOSAL_MESSAGE, msg)
	}
	if err := cbi.processQC(proposal.JustifyQc); err != nil {
		cbi.logger.Errorf("%s", err)
		return
	}
	if proposal.Level != cbi.smr.getCurrentLevel() {
		cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal proposal [%v:%v] does not match the "+
			"smr level [%v:%v], ignore proposal", cbi.selfIndexInEpoch, proposal.Height, proposal.Level,
			cbi.smr.getHeight(), cbi.smr.getCurrentLevel())
		// remove the return value,
		// 因为每个高度可以有多个不同level的区块，<block - timeOut - block' - timeOut - block''> 但不同level的区块包含的QC相同，
		// 这种设计会导致，不同的level的区块，把节点推进到相同的共识状态：qcHeight+1:qcLevel+1, 等待其它对height中的某个区块投票，生成
		// 新的QC'，将所有节点状态推进至QC'+1
		// Note: 这种设计，会导致在相同的level出现不同的高度的区块，因为接收的提案level存在 > qcLevel + 1的可能(超时情况下)；此时依据该qc
		// 节点只能推进到 qcHeight:qcLevel+1状态，但如果在该proposal高度有过超时，则节点的currLevel必定大于 qcLevel+1
	}

	//step5: add proposal to msg pool and add block to chainStore
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal step4 add proposal to msg pool and "+
		"add proposal block to chainStore start", cbi.selfIndexInEpoch)
	if err := cbi.insertProposal(msg); err != nil {
		cbi.logger.Errorf("%s", err)
		return
	}
	executorErr := cbi.chainStore.insertBlock(proposal.GetBlock(), proposal.Level)
	if executorErr != nil {
		cbi.logger.Errorf("service selfIndexInEpoch [%v] processProposal add proposal block %v to chainStore "+
			"failed, err: %s", cbi.selfIndexInEpoch, proposal.GetBlock().GetHeader().BlockHeight, executorErr)
	}
	if qc := cbi.chainStore.getQCById(string(proposal.GetBlock().GetHeader().BlockHash)); qc != nil {
		cbi.chainStore.addQCToBlk(qc)
	}
}

func (cbi *ConsensusMaxBftImpl) voteForProposal(proposal *maxbftpb.ProposalData) {
	// send vote with the proposal to next proposer in the epoch
	// 当提案level小于等于节点的最新投票level时，表示当前节点已经在前述level上进行过投票，可能为赞成票或者超时票。
	if lastVoteLevel, _ := cbi.smr.getLastVote(); lastVoteLevel < proposal.Level {
		if err := cbi.generateVoteAndSend(proposal); err != nil {
			cbi.logger.Errorf("%s", err)
		}
	} else {
		cbi.logger.Debugf("don't vote for proposal: [%d:%d:%x], lastVoteLevel: %d",
			proposal.Height, proposal.Level, proposal.Block.Header.BlockHash, lastVoteLevel)
	}
}

func (cbi *ConsensusMaxBftImpl) fetchDataIfRequire(
	proposalMsg *maxbftpb.ProposalMsg) (needFetch bool, err error) {
	if isFetch, err := cbi.needFetch(proposalMsg.SyncInfo); err != nil {
		return false, err
	} else if !isFetch {
		return false, nil
	}
	cbi.fetchData(proposalMsg.ProposalData)
	return true, nil
}

func (cbi *ConsensusMaxBftImpl) generateVoteAndSend(proposal *maxbftpb.ProposalData) error {
	cbi.smr.updateState(maxbftpb.ConsStateType_VOTE)
	if !cbi.doneReplayWal {
		return nil
	}
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal step5 construct vote and "+
		"send vote to next proposer start", cbi.selfIndexInEpoch)
	vote, err := cbi.constructVote(proposal.Height,
		proposal.Level, cbi.smr.getEpochId(proposal.Height), proposal.GetBlock())
	if err != nil {
		return err
	}
	cbi.sendVote2Next(proposal, vote)
	return nil
}

func (cbi *ConsensusMaxBftImpl) fetchData(proposal *maxbftpb.ProposalData) {
	cbi.logger.Infof("service selfIndexInEpoch [%v] validateProposal need sync up to [%v:%v]",
		cbi.selfIndexInEpoch, proposal.JustifyQc.Height, proposal.JustifyQc.Level)

	//fetch block and qc from proposer
	req := &blockSyncReq{
		targetPeer: proposal.ProposerIdx,
		blockID:    proposal.JustifyQc.BlockId,
		height:     proposal.JustifyQc.Height,
	}
	cbi.syncer.blockSyncReqC <- req
}

// processQC insert qc and process qc from proposal msg
func (cbi *ConsensusMaxBftImpl) processQC(qc *maxbftpb.QuorumCert) error {
	cbi.logger.Debugf("process proposal.JustifyQc start. qc.height:[%d], qc.level:[%d],"+
		" qc.blockHash:[%x], qc.NewView: [%v]", qc.Height, qc.Level, qc.BlockId, qc.NewView)
	if !qc.NewView {
		curEpochId := cbi.smr.getEpochId(qc.Height)
		if err := cbi.chainStore.insertQC(qc, curEpochId); err != nil {
			return fmt.Errorf("insert qc to chainStore failed: %s, qc info: %s", err, qc.String())
		}
		cbi.logger.Debugf("insert qc success")
		cbi.chainStore.addQCToBlk(qc)
	}

	//local already handle it when aggregating qc
	cbi.commitBlocksByQC(qc)
	cbi.processCertificates(qc, nil)
	return nil
}

func (cbi *ConsensusMaxBftImpl) validateBlock(proposal *maxbftpb.ProposalData) error {
	var (
		err      error
		preBlock *common.Block
	)
	if preBlock, err = cbi.chainStore.getBlock(string(
		proposal.Block.Header.PreBlockHash), proposal.Height-1); err != nil || preBlock == nil {
		return fmt.Errorf("service selfIndexInEpoch [%v] validateProposal failed to get preBlock [%v], err %v",
			cbi.selfIndexInEpoch, proposal.Height-1, err)
	}
	if !bytes.Equal(preBlock.Header.BlockHash, proposal.JustifyQc.BlockId) {
		return fmt.Errorf("service selfIndexInEpoch [%v] validateProposal failed, qc'block not equal "+
			"to block's preHash", cbi.selfIndexInEpoch)
	}
	if err = cbi.blockVerifier.VerifyBlock(proposal.Block, protocol.CONSENSUS_VERIFY); err != nil {
		return fmt.Errorf("service selfIndexInEpoch [%v] validateProposal "+
			"[%d:%d:%d] failed: %s", cbi.selfIndexInEpoch, proposal.ProposerIdx,
			proposal.Height, proposal.Level, err)
	}
	cbi.logger.Debugf("verify proposal block[%d:%d:%x] success",
		proposal.Height, proposal.Level, proposal.Block.Header.BlockHash)
	return nil
}

func (cbi *ConsensusMaxBftImpl) validateBlockConsensusArg(block *common.Block, level uint64) error {
	var (
		err     error
		txRWSet *common.TxRWSet
	)
	if txRWSet, err = governance.CheckAndCreateGovernmentArgs(cbi.proposalCache, block,
		cbi.governanceContract); err != nil {
		return fmt.Errorf("validateBlockConsensusArg: CheckAndCreateGovernmentArgs err "+
			"at height [%v:%x], err %v", block.Header.BlockHeight, block.Header.BlockHash, err)
	}
	consensusArgs := &consensus.BlockHeaderConsensusArgs{
		ConsensusType: int64(consensus.ConsensusType_MAXBFT),
		Level:         level,
		ConsensusData: txRWSet,
	}
	consensusData, _ := proto.Marshal(consensusArgs)
	if !bytes.Equal(consensusData, block.Header.ConsensusArgs) {
		return fmt.Errorf("validateBlockConsensusArg: invalid Consensus Args "+
			"at height [%v:%x], block consensucData:[%v] local data:[%v]",
			block.Header.BlockHeight, block.Header.BlockHash,
			sha256.Sum256(block.Header.ConsensusArgs), sha256.Sum256(consensusData))
	}
	cbi.logger.Debugf("validateBlockConsensusArg success")
	return nil
}

func (cbi *ConsensusMaxBftImpl) sendVote2Next(proposal *maxbftpb.ProposalData,
	vote *maxbftpb.ConsensusPayload) {
	nextLeaderIndex := cbi.getProposer(proposal.Height, proposal.Level+1)

	cbi.logger.Debugf("service selfIndexInEpoch [%v] processProposal send vote to next leader [%v]",
		cbi.selfIndexInEpoch, nextLeaderIndex)
	cbi.smr.setLastVote(vote, proposal.Level)
	if nextLeaderIndex == cbi.selfIndexInEpoch {
		consensusMessage := &maxbftpb.ConsensusMsg{Payload: vote}
		if err := utils.SignConsensusMsg(consensusMessage, cbi.chainConf.ChainConfig().Crypto.Hash,
			cbi.singer); err != nil {
			cbi.logger.Errorf("sign consensus message failed, err %v", err)
			return
		}
		cbi.logger.Debugf("send vote msg to self[%d], voteHeight:[%d], voteLevel:[%d], voteBlockId:[%x]",
			cbi.selfIndexInEpoch, proposal.Height, proposal.Level, proposal.Block.Header.BlockHash)
		cbi.internalMsgCh <- consensusMessage
	} else {
		cbi.logger.Debugf("send vote msg to other peer [%d], voteHeight:[%d], voteLevel:[%d], voteBlockID:[%x]",
			nextLeaderIndex, proposal.Height, proposal.Level, proposal.Block.Header.BlockHash)
		cbi.signAndSendToPeer(vote, proposal.Height, nextLeaderIndex)
	}
}

func (cbi *ConsensusMaxBftImpl) validateVoteMsg(msg *maxbftpb.ConsensusMsg) error {
	var (
		peer    *peer
		voteMsg = msg.Payload.GetVoteMsg()
		author  = voteMsg.VoteData.GetAuthor()
	)
	if author == nil {
		return fmt.Errorf("validateVoteMsg: received a vote msg with nil author")
	}
	if err := cbi.validateSignature(msg, peer); err != nil {
		return fmt.Errorf("validateVoteMsg failed, vote %v, err %v", voteMsg, err)
	}
	vote := voteMsg.VoteData
	vote.Signature.Signer = msg.SignEntry.Signer
	return nil
}

func (cbi *ConsensusMaxBftImpl) verifyVote(vote *maxbftpb.VoteData) error {
	validators := cbi.smr.getPeers(vote.Height)
	members := make([]*consensus.GovernanceMember, 0, len(validators))
	for _, v := range validators {
		members = append(members, &consensus.GovernanceMember{
			NodeId: v.id, Index: v.index,
		})
	}
	return validateVoteData(vote, members, cbi.acp)
}

func (cbi *ConsensusMaxBftImpl) processVote(msg *maxbftpb.ConsensusMsg) {
	var (
		voteMsg   = msg.Payload.GetVoteMsg()
		vote      = msg.Payload.GetVoteMsg().VoteData
		authorIdx = vote.GetAuthorIdx()
	)

	cbi.logger.DebugDynamic(func() string {
		return fmt.Sprintf("service selfIndexInEpoch [%v] processVote: voteAuthor:[%v]"+
			" voteBaseInfo:[%d:%d:%d], expected:[%v:%v:%v], voteMsg: %s", cbi.selfIndexInEpoch,
			authorIdx, vote.Height, vote.Level, vote.EpochId, cbi.smr.getHeight(),
			cbi.smr.getCurrentLevel(), cbi.smr.getEpochId(vote.Height), vote.String())
	})

	// 0. add vote
	insert, err := cbi.insertVote(vote)
	if err != nil || !insert {
		cbi.logger.Warnf("failed add vote: %s, error: %s, insert result: %v", vote.String(), err, insert)
		return
	}

	if cbi.doneReplayWal {
		cbi.addProposalWalIndex(vote.Height)
		cbi.saveWalEntry(maxbftpb.MessageType_VOTE_MESSAGE, msg)
	}

	// Note: 检查应该放在insertVote 后面，以便有效的投票可以添加到投票池中
	if vote.Level < cbi.smr.getCurrentLevel() || vote.EpochId != cbi.smr.getEpochId(vote.Height) {
		cbi.logger.Warnf("service selfIndexInEpoch [%v] processVote: received vote at wrong level or epoch",
			cbi.selfIndexInEpoch)
		return
	}

	// 1. whether need fetch data from vote peer.
	if err := cbi.fetchByVoteIfRequire(voteMsg); err != nil {
		cbi.logger.Debugf("%s", err)
		return
	}

	var (
		qc      = voteMsg.SyncInfo.HighestQc
		tc      = voteMsg.SyncInfo.HighestTc
		certifi = qc
		useQc   = true
	)
	if tc != nil && qc.Level < tc.Level {
		certifi = tc
		useQc = false
	}
	storedQc := cbi.chainStore.getQCById(string(qc.BlockId))
	if useQc && storedQc == nil || !useQc {
		if err := cbi.verifyJustifyQC(certifi); err != nil {
			cbi.logger.Warnf("verify QuorumCert[%d:%x] isTc:%v, qcBlockId:%x, error: %+v",
				certifi.Height, certifi.Level, certifi.BlockId != nil, certifi.BlockId, err)
			return
		}

		// 使用投票携带的QC/TC更新节点状态；为了解决节点可能中途由于网络问题，导致部分QC/TC未即使收到，导致节点状态未更新至最新
		// 当投票中只携带QC，或携带的QC>TC时，使用QC去尝试推进节点状态
		// 当TC大时，则用TC更新节点状态
		if useQc {
			curEpochId := cbi.smr.getEpochId(qc.Height)
			if err := cbi.chainStore.insertQC(qc, curEpochId); err == nil {
				cbi.processCertificates(qc, nil)
			}
		} else if cbi.smr.getHighestTCLevel() < tc.Level {
			cbi.processCertificates(nil, tc)
		}
	}
	// 5. generate QC if majority are voted and process the new QC if don't need sync data from peers
	cbi.logger.Debugf("process vote step 4 no need fetch info and process vote")
	cbi.processVotes(vote)
}

func (cbi *ConsensusMaxBftImpl) fetchByVoteIfRequire(voteMsg *maxbftpb.VoteMsg) (err error) {
	if need, err := cbi.needFetch(voteMsg.SyncInfo); err != nil {
		return fmt.Errorf("check need fetch data failed, reason: %s", err)
	} else if !need {
		return nil
	}
	cbi.fetch(voteMsg.VoteData.AuthorIdx, voteMsg)
	return fmt.Errorf("is fetching data from %d", voteMsg.VoteData.AuthorIdx)
}

func (cbi *ConsensusMaxBftImpl) insertVote(vote *maxbftpb.VoteData) (insert bool, err error) {
	if inserted, err := cbi.msgPool.InsertVote(vote); err != nil {
		return false, fmt.Errorf("insert vote msg failed, err %v, "+
			"insert %v, authorIdx: %d ", err, inserted, vote.AuthorIdx)
	} else if !inserted {
		return false, nil
	}
	return true, nil
}

func (cbi *ConsensusMaxBftImpl) insertProposal(msg *maxbftpb.ConsensusMsg) error {
	var proposal = msg.Payload.GetProposalMsg().ProposalData
	if inserted, err := cbi.msgPool.InsertProposal(msg); err != nil || !inserted {
		return fmt.Errorf("insert proposal to msgPool failed,"+
			" reason: %s, insert %v, authorIdx: %d", err, inserted, proposal.ProposerIdx)
	}
	return nil
}

//fetchAndHandleQc Fetch the missing block data and the  process the received QC until the data is all fetched.
func (cbi *ConsensusMaxBftImpl) fetch(authorIdx uint64, voteMsg *maxbftpb.VoteMsg) {
	cbi.logger.Infof("service selfIndexInEpoch [%v] processVote: need sync up to [%v:%v]",
		cbi.selfIndexInEpoch, voteMsg.SyncInfo.HighestQc.Height, voteMsg.SyncInfo.HighestQc.Level)
	req := &blockSyncReq{
		targetPeer: authorIdx,
		height:     voteMsg.SyncInfo.HighestQc.Height,
		blockID:    voteMsg.SyncInfo.HighestQc.BlockId,
	}
	cbi.syncer.blockSyncReqC <- req
}

//processVotes QC is generated if a majority are voted for the special Height and Level.
func (cbi *ConsensusMaxBftImpl) processVotes(vote *maxbftpb.VoteData) {
	blockId, newView, done := cbi.msgPool.CheckVotesDone(vote.Height, vote.Level)
	if !done {
		cbi.logger.Debugf("not done for vote:[%d:%d]", vote.Height, vote.Level)
		return
	}

	var minQuorum int
	if cbi.smr.info.switchEpochHeight != 0 && vote.Height > cbi.smr.info.switchEpochHeight &&
		vote.Height <= cbi.smr.info.switchEpochHeight+3 {
		minQuorum = int(cbi.smr.info.LastMinQuorumForQc)
	} else {
		minQuorum = int(cbi.smr.info.MinQuorumForQc)
	}
	if reach, _ := cbi.verifyAlgo.verifyVotes(
		minQuorum,
		cbi.msgPool.GetQCVotes(vote.Height, vote.Level)); !reach {
		return
	}
	//aggregate qc/tc
	qc, err := cbi.aggregateQC(vote.Height, vote.Level, blockId, newView)
	if err != nil {
		cbi.logger.Errorf("service index [%v] processVote: new qc aggregated for height [%v] "+
			"level [%v] BlockId [%x], err=%v", cbi.selfIndexInEpoch, vote.Height, vote.Level, blockId, err)
		return
	}

	if !qc.NewView {
		//因为在processBlockCommitted环节，节点会使用当前已知的最高QC去更新状态；
		//此时如果使用一个未含有block的QC去推进了节点状态，导致该节点作为leader时既无法生成下一个块，
		//也无法处理请求到的缺失块数据，因为此时节点的状态level大于接收到的丢失数据，不再处理。
		blk := cbi.chainStore.getBlockByHash(qc.BlockId)
		if blk == nil {
			cbi.logger.Warnf("Does not hold the current QC bloc：%d:%x", qc.Height, qc.BlockId)
			return
		}
		cbi.chainStore.addQCToBlk(qc)
		curEpochId := cbi.smr.getEpochId(qc.Height)
		if err := cbi.chainStore.insertQC(qc, curEpochId); err != nil {
			cbi.logger.Errorf("insert qc[%d:%d%x] failed, reason: %s", qc.Height, qc.Level, qc.BlockId, err)
			return
		}
	}
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processVotes: aggregated for height [%v]"+
		" level [%v], newView: %v, qcInfo: %s", cbi.selfIndexInEpoch, vote.Height, vote.Level, newView, qc.String())
	if qc.NewView {
		// If the newly generated QC type is NewView, it means that majority agree on the timeout and assign QC to TC
		cbi.processCertificates(nil, qc)
	} else {
		cbi.processCertificates(qc, nil)
	}
	cbi.smr.updateState(maxbftpb.ConsStateType_PROPOSE)
	if !cbi.doneReplayWal {
		return
	}
	cbi.processNewPropose(cbi.smr.getHeight(), cbi.smr.getCurrentLevel(), cbi.chainStore.getCurrentQC().BlockId)
}

func (cbi *ConsensusMaxBftImpl) aggregateQC(height, level uint64, blockId []byte,
	isNewView bool) (*maxbftpb.QuorumCert, error) {
	votes := cbi.msgPool.GetQCVotes(height, level)
	qc := &maxbftpb.QuorumCert{
		BlockId: blockId,
		Height:  height,
		Level:   level,
		Votes:   votes,
		NewView: isNewView,
		EpochId: cbi.smr.getEpochId(height),
	}
	return qc, nil
}

// processCertificates
// qc When processing a proposalMsg or voteMsg, the tc information is contained in the incoming message;
// in other cases, the parameter is currentQC in local node.
// tc When processing a proposalMsg or voteMsg, the tc information is contained in the incoming message;
// in other cases, the parameter is nil.
func (cbi *ConsensusMaxBftImpl) processCertificates(qc *maxbftpb.QuorumCert, tc *maxbftpb.QuorumCert) {
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processCertificates "+
		"start: smrHeight [%v], smrLevel [%v]", cbi.selfIndexInEpoch, cbi.smr.getHeight(), cbi.smr.getCurrentLevel())
	var committedLevel = cbi.smr.getLastCommittedLevel()
	if tc != nil {
		cbi.smr.updateTC(tc)
	}
	//cbi.logger.Debugf("local node's currentQC: %s", cbi.chainStore.getCurrentQC().String())

	cbi.smr.updateLockedQC(qc)
	//if enterNewLevel := cbi.smr.processCertificates(qc.Height, currentQC.Level, tcLevel,
	//committedLevel); enterNewLevel {
	if enterNewLevel := cbi.smr.processCertificates(qc, tc, committedLevel); enterNewLevel {
		cbi.smr.updateState(maxbftpb.ConsStateType_NEW_HEIGHT)
		cbi.processNewHeight(cbi.smr.getHeight(), cbi.smr.getCurrentLevel())
	}
}

func (cbi *ConsensusMaxBftImpl) commitBlocksByQC(qc *maxbftpb.QuorumCert) {
	cbi.logger.Debugf("service selfIndexInEpoch [%v] commitBlocksByQC start", cbi.selfIndexInEpoch)
	commit, block, level := cbi.smr.commitRules(qc)
	if !commit {
		return
	}
	cbi.logger.Debugf("service selfIndexInEpoch [%v] processCertificates: commitRules success, "+
		"height [%v], level [%v], committed level [%v]",
		cbi.selfIndexInEpoch, block.Header.BlockHeight, level, cbi.chainStore.getCommitLevel())
	if level > cbi.chainStore.getCommitLevel() {
		cbi.logger.Debugf("service selfIndexInEpoch [%v] processCertificates: try committing "+
			"a block %v on [%x:%v]", cbi.selfIndexInEpoch, block.Header.BlockHash, block.Header.BlockHeight, level)
		lastCommittedBlock, lastCommitLevel, err := cbi.chainStore.commitBlock(block)
		if lastCommittedBlock != nil {
			cbi.processCommittedBlock(lastCommittedBlock, lastCommitLevel)
			cbi.updateWalIndexAndTrunc(lastCommittedBlock.Header.BlockHeight)
		}
		if err != nil {
			cbi.logger.Errorf("commit block to the chain failed, reason: %s", err)
		}
	}
}

func (cbi *ConsensusMaxBftImpl) processCommittedBlock(block *common.Block, blkLevel uint64) {
	cbi.logger.Debugf("processBlockCommitted received has committed block, height:%d, hash:%x",
		block.Header.BlockHeight, block.Header.BlockHash)
	// 1. check base commit block info
	if cbi.commitHeight >= block.Header.BlockHeight {
		cbi.logger.Warnf("service selfIndexInEpoch [%v] block:[%d:%x] has been committed",
			cbi.selfIndexInEpoch, block.Header.BlockHeight, block.Header.BlockHash)
		return
	}
	cbi.logger.Debugf("setCommit block status")
	cbi.smr.setLastCommittedLevel(blkLevel)
	cbi.logger.Debugf("on block sealed, blockHeight: %d", block.Header.BlockHeight)
	cbi.msgPool.OnBlockSealed(block.Header.BlockHeight)
	// 2. insert committed block to chainStore
	cbi.logger.Debugf("processBlockCommitted step 1 insert complete block")
	if err := cbi.chainStore.insertCompletedBlock(block); err != nil {
		cbi.logger.Errorf("insert block[%d:%x] to chainStore failed, reason: %s",
			block.Header.BlockHeight, block.Header.BlockHash, err)
		return
	}
	// 3. update commit info in the consensus
	cbi.logger.Debugf("processBlockCommitted step 2 update the last committed block info")
	cbi.commitHeight = block.Header.BlockHeight
	// 4. create next epoch if meet the condition
	cbi.logger.Debugf("processBlockCommitted step 3 create epoch if meet the condition")
	epoch, err := cbi.createNextEpochIfRequired(cbi.commitHeight, blkLevel)
	if err != nil {
		cbi.logger.Warnf("create epoch failed, reason: %s", err)
		return
	}
	// 5. check if need to switch with the epoch
	if epoch == nil || epoch.switchHeight > cbi.commitHeight {
		cbi.logger.Debugf("processBlockCommitted step 4 no switch epoch and process qc")
		cbi.processCertificates(cbi.chainStore.getCurrentQC(), nil)
		return
	}
	// 6. switch epoch and update field state in consensus
	oldIndex := cbi.selfIndexInEpoch
	cbi.logger.Debugf("processBlockCommitted step 5 switch epoch and process qc")
	cbi.switchNextEpoch(cbi.commitHeight, epoch)
	if cbi.smr.isValidIdx(cbi.selfIndexInEpoch, cbi.commitHeight) {
		cbi.logger.Infof("service selfIndexInEpoch [%v] start processCertificates,"+
			"height [%v],level [%v]", cbi.selfIndexInEpoch, cbi.smr.getHeight(), cbi.smr.getCurrentLevel())
	} else if oldIndex != cbi.selfIndexInEpoch {
		if oldIndex == utils.InvalidIndex {
			cbi.logger.Infof("service selfIndexInEpoch [%v] got a chance to join consensus group",
				cbi.selfIndexInEpoch)
		} else {
			cbi.logger.Infof("service old selfIndexInEpoch [%v] next selfIndexInEpoch [%v]"+
				" leave consensus group",
				oldIndex, cbi.selfIndexInEpoch)
		}
	}
	cbi.processCertificates(cbi.chainStore.getCurrentQC(), nil)
	cbi.logger.Infof("processBlockCommitted end, block: [%d:%x].", cbi.commitHeight, block.Header.BlockHash)
}

func (cbi *ConsensusMaxBftImpl) processBlockCommitted(blk *common.Block) {
	blkqc := cbi.chainStore.getQCById(string(blk.Header.BlockHash))
	if blkqc == nil {
		cbi.logger.Infof("process committed block:[%d:%x] but not "+
			"find its' qc info", blk.Header.BlockHeight, blk.Header.BlockHash)
		qc := utils.GetQCFromBlock(blk)
		if len(qc) == 0 {
			cbi.logger.Warnf("not find qc from block:[%d:%x]",
				blk.Header.BlockHeight, blk.Header.BlockHash)
			return
		}
		blkqc = new(maxbftpb.QuorumCert)
		err := proto.Unmarshal(qc, blkqc)
		if err != nil {
			cbi.logger.Errorf("process committed block:[%d:%x] failed, ummarshal qc error:%+v, qc:%+v", err, qc)
			return
		}
	}
	cbi.processCommittedBlock(blk, blkqc.Level)
	cbi.updateWalIndexAndTrunc(blk.Header.BlockHeight)
}

func (cbi *ConsensusMaxBftImpl) switchNextEpoch(blockHeight uint64, epoch *epochManager) {
	cbi.logger.Debugf("service [%v] handle block committed: "+
		"start switching to next epoch at height [%v]", cbi.selfIndexInEpoch, blockHeight)
	cbi.selfIndexInEpoch = epoch.index
	if epoch.governanceContract.EpochId > cbi.governanceContract.EpochId {
		cbi.governanceContract = epoch.governanceContract
	}
	cbi.initTimeOutConfig(epoch.governanceContract)
	cbi.smr.updateContractInfo(epoch)
	cbi.smr.setEpochId(epoch.epochId)
	cbi.msgPool.UpdateMinVotesForQc(epoch.msgPool)
	cbi.smr.paceMaker.SetupTimeout()
}

func (cbi *ConsensusMaxBftImpl) validateBlockFetch(msg *maxbftpb.ConsensusMsg) error {
	req := msg.Payload.GetBlockFetchMsg()
	authorIdx := req.GetAuthorIdx()
	peer := cbi.smr.getPeerByIndex(req.Height, authorIdx)
	if peer == nil {
		return fmt.Errorf("validateBlockFetch: received a vote msg from invalid peer: %d", authorIdx)
	}
	return cbi.validateSignature(msg, peer)
}

func (cbi *ConsensusMaxBftImpl) processBlockFetch(msg *maxbftpb.ConsensusMsg) {
	var (
		req       = msg.Payload.GetBlockFetchMsg()
		authorIdx = req.GetAuthorIdx()
	)

	cbi.logger.Debugf("receive req msg:%s, authorIDx: %d", req.String(), authorIdx)
	if err := cbi.validateBlockFetch(msg); err != nil {
		cbi.logger.Warnf("verify msg failed: %s", err)
		return
	}
	blocks, status := cbi.fetchBlocks(req)
	if len(blocks) == 0 {
		return
	}
	cbi.logger.Debugf("response blocks num: %d", len(blocks))
	reps := cbi.newResponses(blocks, status, req.ReqId)
	for i := range reps {
		cbi.signAndSendToPeer(reps[i], cbi.smr.getHeight(), authorIdx)
	}
}

func (cbi *ConsensusMaxBftImpl) fetchBlocks(req *maxbftpb.BlockFetchMsg) (
	[]*maxbftpb.BlockPair, maxbftpb.BlockFetchStatus) {
	// 1. fetch data ...
	var (
		height      = req.Height
		currBlkHash = req.BlockId

		status = maxbftpb.BlockFetchStatus_SUCCEEDED
		blocks = make([]*maxbftpb.BlockPair, 0, req.NumBlocks)
	)
	for !bytes.Equal(currBlkHash, req.CommitBlock) && !bytes.Equal(currBlkHash, req.LockedBLock) {
		qc, _ := cbi.chainStore.getQC(string(currBlkHash), height)
		block, _ := cbi.chainStore.getBlock(string(currBlkHash), height)
		if block == nil || qc == nil || !bytes.Equal(qc.BlockId, block.Header.BlockHash) {
			cbi.logger.Debugf("not found block:[%v] or qc info:[%v] in [%d:%x]", block, qc)
			status = maxbftpb.BlockFetchStatus_NOT_ENOUGH_BLOCKS
			break
		}
		height = height - 1
		currBlkHash = block.Header.PreBlockHash
		blocks = append(blocks, &maxbftpb.BlockPair{
			Block: block, Qc: qc,
		})
		//cbi.logger.Debugf("found block: [%d:%x]", block.Header.BlockHeight, block.Header.BlockHash)
	}
	if len(blocks) == 0 {
		return nil, status
	}
	sort.Slice(blocks, func(i, j int) bool {
		return blocks[i].Block.Header.BlockHeight < blocks[j].Block.Header.BlockHeight
	})
	return blocks, status
}

func (cbi *ConsensusMaxBftImpl) newResponses(blocks []*maxbftpb.BlockPair,
	status maxbftpb.BlockFetchStatus, reqId uint64) []*maxbftpb.ConsensusPayload {

	count := len(blocks) / MaxSyncBlockNum
	if len(blocks)%MaxSyncBlockNum > 0 {
		count++
	}
	resp := make([]*maxbftpb.ConsensusPayload, 0, count)
	for i := 0; i <= count-1; i++ {
		if i == count-1 {
			resp = append(resp, cbi.constructBlockFetchRespMsg(
				blocks[i*MaxSyncBlockNum:], status, reqId))
		} else {
			resp = append(resp, cbi.constructBlockFetchRespMsg(
				blocks[i*MaxSyncBlockNum:(i+1)*MaxSyncBlockNum], status, reqId))
		}
	}
	return resp
}

func (cbi *ConsensusMaxBftImpl) validateBlockFetchRsp(msg *maxbftpb.ConsensusMsg) error {
	rsp := msg.Payload.GetBlockFetchRespMsg()
	authorIdx := rsp.GetAuthorIdx()
	peer := cbi.smr.getPeerByIndex(0, authorIdx)
	if peer == nil {
		cbi.logger.Errorf("service selfIndexInEpoch [%v] validateBlockFetchRsp: "+
			"received a vote msg from invalid peer", cbi.selfIndexInEpoch)
		return ErrInvalidPeer
	}

	if err := cbi.validateSignature(msg, peer); err != nil {
		cbi.logger.Errorf("service selfIndexInEpoch [%v] from %v validateBlockFetchRsp failed, err %v"+
			" fetch rsp %v, err %v", cbi.selfIndexInEpoch, rsp.AuthorIdx, rsp, err)
		return ErrValidateSign
	}
	cbi.logger.Infof("service selfIndexInEpoch [%v] from %v validateBlockFetchRsp success,",
		cbi.selfIndexInEpoch, rsp.AuthorIdx)
	return nil
}

func (cbi *ConsensusMaxBftImpl) addTimerEvent(event *timeservice.TimerEvent) {
	cbi.timerService.AddEvent(event)
}

//validateSignature validate msg signatures
func (cbi *ConsensusMaxBftImpl) validateSignature(msg *maxbftpb.ConsensusMsg, peer *peer) error {
	//todo. will delete, 因为验签失败，加入一行log，记录验签时的数据哈希值；
	// 同时，在数据签名的位置，也有一行日志，记录原始数据签名时的哈希值
	if err := utils.VerifyConsensusMsgSign(msg, cbi.acp); err != nil {
		return fmt.Errorf("verify signature failed, error: %s, sigHash: %x",
			err, sha256.Sum256(msg.SignEntry.Signature))
	}
	return nil
}
