/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package maxbft

import (
	"crypto/sha256"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/net"

	"chainmaker.org/chainmaker/common/v2/msgbus"
	"chainmaker.org/chainmaker/consensus-maxbft/v2/utils"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"github.com/gogo/protobuf/proto"
)

//signAndMarshal signs the consensus payload and marshal consensus message including signature
func (cbi *ConsensusMaxBftImpl) signAndMarshal(payload *maxbftpb.ConsensusPayload,
	internal bool) ([]byte, error) {
	consensusMessage := &maxbftpb.ConsensusMsg{
		Payload:   payload,
		SignEntry: nil,
	}
	if err := utils.SignConsensusMsg(consensusMessage, cbi.chainConf.ChainConfig().Crypto.Hash, cbi.singer); err != nil {
		return nil, fmt.Errorf("sign consensus message failed, err %v", err)
	}
	cbi.logger.DebugDynamic(func() string {
		data, _ := proto.Marshal(payload)
		return fmt.Sprintf("The hash of the unsigned raw data when sign data：%x", sha256.Sum256(data))
	})
	cbi.logger.DebugDynamic(func() string {
		return fmt.Sprintf("signature hash: %x when sign data",
			sha256.Sum256(consensusMessage.SignEntry.Signature))
	})
	cbi.logger.DebugDynamic(func() string {
		bz, err := consensusMessage.SignEntry.Signer.Marshal()
		if err != nil {
			panic(err)
		}
		return fmt.Sprintf("signer hash: %x when sign data", sha256.Sum256(bz))
	})
	if internal {
		//send it to self, no need to marshal
		//but msg will be updated in validateInsertVote, so clone it to avoid Marshal error
		msg, _ := proto.Clone(consensusMessage).(*maxbftpb.ConsensusMsg)
		cbi.internalMsgCh <- msg
	}
	consensusData, err := proto.Marshal(consensusMessage)
	if err != nil {
		return nil, fmt.Errorf("marshal consensus message failed, err %v", err)
	}
	return consensusData, nil
}

//Broadcast signs the consensus message and broadcasts it to consensus group
func (cbi *ConsensusMaxBftImpl) Broadcast(height uint64, data []byte) {
	peers := cbi.smr.peers(height)
	for _, peer := range peers {
		if peer.index == int64(cbi.selfIndexInEpoch) {
			continue
		}
		msg := &net.NetMsg{
			Payload: data,
			Type:    net.NetMsg_CONSENSUS_MSG,
			To:      peer.id,
		}
		go cbi.msgbus.Publish(msgbus.SendConsensusMsg, msg)
	}
}

//signAndSendToPeer signs the consensus message and unicast it to the specified peer
func (cbi *ConsensusMaxBftImpl) signAndSendToPeer(payload *maxbftpb.ConsensusPayload, blkHeight, index uint64) {
	consensusData, err := cbi.signAndMarshal(payload, false)
	if err != nil {
		cbi.logger.Errorf("sign payload failed, err %v", err)
		return
	}
	cbi.sendToPeer(consensusData, blkHeight, index)
}

//sendToPeer sends consensus data to specified peer
func (cbi *ConsensusMaxBftImpl) sendToPeer(consensusData []byte, blkHeight, index uint64) {
	peer := cbi.smr.getPeerByIndex(blkHeight, index)
	if peer == nil {
		cbi.logger.Errorf("get peer with index %v failed", cbi.selfIndexInEpoch, index)
		return
	}
	msg := &net.NetMsg{
		To:      peer.id,
		Type:    net.NetMsg_CONSENSUS_MSG,
		Payload: consensusData,
	}
	go cbi.msgbus.Publish(msgbus.SendConsensusMsg, msg)
}
