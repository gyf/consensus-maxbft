module chainmaker.org/chainmaker/consensus-maxbft/v2

go 1.15

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.2.0
	chainmaker.org/chainmaker/common/v2 v2.2.0
	chainmaker.org/chainmaker/consensus-utils/v2 v2.2.0
	chainmaker.org/chainmaker/logger/v2 v2.2.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.0
	chainmaker.org/chainmaker/protocol/v2 v2.2.0
	chainmaker.org/chainmaker/utils/v2 v2.2.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/panjf2000/ants/v2 v2.4.7
	github.com/spf13/viper v1.10.1 // indirect
	github.com/stretchr/testify v1.7.0
)
