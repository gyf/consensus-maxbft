package message

import (
	"testing"

	"github.com/gogo/protobuf/proto"

	"github.com/stretchr/testify/require"

	"chainmaker.org/chainmaker/utils/v2"

	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
)

func TestVotePool_checkDuplicationVote(t *testing.T) {
	pool := newVotePool(4)
	votes := generateVote(4)

	// 1. check new votes shouldn't be duplicate
	for _, v := range votes {
		valid, err := pool.checkDuplicationVote(v)
		require.NoError(t, err)
		require.True(t, valid)
	}

	// 2. add vote and check duplicate
	add, err := pool.insertVote(votes[1], 3)
	require.NoError(t, err)
	require.True(t, add)

	vote2 := proto.Clone(votes[1]).(*maxbftpb.VoteData)
	valid, err := pool.checkDuplicationVote(vote2)
	require.NoError(t, err)
	require.Falsef(t, valid, "duplicate vote")

	// 3. modify vote's blockId for same level and check should be error
	vote2.BlockId = []byte(utils.GetRandTxId())
	valid, err = pool.checkDuplicationVote(vote2)
	require.Error(t, err)
	require.False(t, valid)
}

func TestVotePool_addVoteIfNeed(t *testing.T) {
	pool := newVotePool(4)
	votes := generateVote(4)

	// 0. check two votes properties
	require.True(t, votes[0].NewView, "first vote should be true for newView")
	require.Truef(t, len(votes[0].BlockId) == 0, "first vote not have block id")
	require.False(t, votes[1].NewView, "second vote should be false for newView")
	require.Truef(t, len(votes[1].BlockId) > 0, "second vote should have block id")

	// ------ first receive NewView vote from user in the state
	// ------ after receive BlockId vote from same user and same state
	// 1. add first vote: newView vote
	require.Truef(t, pool.addVoteIfNeed(votes[0]), "first add newView vote should be success")

	// 2. repeat add first vote: newView vote
	require.Falsef(t, pool.addVoteIfNeed(votes[0]), "repeat add same vote should be fail")

	// 3. update first vote property: add blockID and added to pool
	vote1 := proto.Clone(votes[0]).(*maxbftpb.VoteData)
	vote1.BlockId = []byte(utils.GetRandTxId())
	require.Truef(t, pool.addVoteIfNeed(vote1), "should add vote success due to add blockId property")

	// ------ first receive BlockId vote from user in the state
	// ------ after receive NewView vote from same user and same state
	// 4. add first vote: newView vote
	require.Truef(t, pool.addVoteIfNeed(votes[1]), "first add blockID vote should be success")

	// 5. repeat add first vote: newView vote
	require.Falsef(t, pool.addVoteIfNeed(votes[1]), "repeat add same vote should be fail")

	// 6. update first vote property: add newView and added to pool
	vote2 := proto.Clone(votes[1]).(*maxbftpb.VoteData)
	vote2.NewView = true
	require.Truef(t, pool.addVoteIfNeed(vote2), "should add vote success due to add newView property")
}

func TestVotePool_CheckDone_ForNewView(t *testing.T) {
	pool := newVotePool(4)
	votes := generateVote(4)

	//=======test newView is done when receive 3 votes and quorum is 3=======

	// 1. modify vote1 info to newView
	votes[1].BlockId = nil
	votes[1].NewView = true

	// 2. insert 3 newView votes
	for i := 0; i < 3; i++ {
		add, err := pool.insertVote(votes[i], 3)
		require.True(t, add)
		require.NoError(t, err)
	}

	// 3. test whether is done
	blkId, newView, done := pool.checkVoteDone()
	require.Nil(t, blkId)
	require.True(t, done)
	require.True(t, newView)
}

func TestVotePool_InsertVote_CheckVoteDone(t *testing.T) {
	pool := newVotePool(4)
	votes := generateVote(4)

	for i := range votes {
		added, err := pool.insertVote(votes[i], 3)
		require.NoError(t, err)
		require.Truef(t, added, "should add vote success")
	}

	// 1. check vote properties and update these properties
	require.True(t, votes[0].NewView, "first vote should be true for newView")
	require.Truef(t, len(votes[0].BlockId) == 0, "first vote not have block id")
	require.False(t, votes[1].NewView, "second vote should be false for newView")
	require.Truef(t, len(votes[1].BlockId) > 0, "second vote should have block id")
	vote1 := proto.Clone(votes[0]).(*maxbftpb.VoteData)
	vote2 := proto.Clone(votes[1]).(*maxbftpb.VoteData)
	vote1.BlockId = vote2.BlockId
	vote2.NewView = true

	// 2. Add a Vote with updated attributes
	added, err := pool.insertVote(vote2, 3)
	require.NoError(t, err)
	require.Truef(t, added, "should add vote success")

	// 3. Check that enough NewView votes have been collected
	blkId, newView, done := pool.checkVoteDone()
	require.True(t, len(blkId) == 0)
	require.Truef(t, newView, "should be newView state")
	require.Truef(t, done, "should be done for the level status")

	newViewQc := pool.getQCVotes()
	require.EqualValues(t, 3, len(newViewQc))
	for _, v := range newViewQc {
		require.True(t, v.NewView)
	}

	// 4. Add a Vote with updated attributes
	added, err = pool.insertVote(vote1, 3)
	require.NoError(t, err)
	require.Truef(t, added, "should add vote success")

	// 5. Check that enough BlockId votes have been collected
	blkId, newView, done = pool.checkVoteDone()
	require.Truef(t, len(blkId) > 0, "also should have blockId state")
	require.False(t, newView)
	require.True(t, done)

	blkQc := pool.getQCVotes()
	require.EqualValues(t, 3, len(blkQc))
	for _, v := range blkQc {
		require.True(t, len(v.BlockId) > 0)
	}
}

func TestVotePool_delVote(t *testing.T) {
	pool := newVotePool(4)
	votes := generateVote(4)

	// 1. insert votes
	for i := range votes {
		added, err := pool.insertVote(votes[i], 3)
		require.NoError(t, err)
		require.Truef(t, added, "should add vote success")
	}

	// 2. delete votes
	for i := range votes {
		pool.delVote(votes[i])
	}
	require.EqualValues(t, 0, len(pool.votes))
	require.EqualValues(t, 0, len(pool.votedNewView))
	require.EqualValues(t, 0, len(pool.votedBlockId[string(votes[1].BlockId)]))
}

func generateVote(num int) []*maxbftpb.VoteData {
	blkId := []byte(utils.GetRandTxId())
	votes := make([]*maxbftpb.VoteData, 0, num)
	for i := 0; i < num; i++ {
		blockId := blkId
		if i%2 == 0 {
			blockId = nil
		}
		votes = append(votes, &maxbftpb.VoteData{
			BlockId:   blockId,
			Height:    uint64(i + 10),
			AuthorIdx: uint64(i % 4),
			NewView:   i%2 == 0,
		})
	}
	return votes
}
