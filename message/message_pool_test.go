/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package message

import (
	"testing"

	"github.com/stretchr/testify/require"

	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/utils/v2"
)

func TestMsgPool_InsertVote(t *testing.T) {
	voteNewView := maxbftpb.VoteData{
		Level: 1, Height: 1, AuthorIdx: 3, NewView: true,
	}
	voteBlock := maxbftpb.VoteData{
		Level: 1, Height: 2, AuthorIdx: 3, BlockId: []byte(utils.GetRandTxId()),
	}

	pool := NewMsgPool(4, 4, 3)
	add, err := pool.InsertVote(&voteBlock)
	require.True(t, add)
	require.NoError(t, err)
	add, err = pool.InsertVote(&voteNewView)
	require.True(t, add)
	require.NoError(t, err)

	// check last valid round
	require.True(t, pool.GetLastValidRound(1) < 0)
	require.EqualValues(t, 0, len(pool.GetQCVotes(1, 1)))

	require.False(t, pool.CheckAnyVotes(1, 1))
	require.False(t, pool.CheckAnyVotes(8, 1))

	_, _, done := pool.CheckVotesDone(1, 1)
	require.False(t, done)
	_, _, done = pool.CheckVotesDone(8, 1)
	require.False(t, done)
	// cleanup status
	pool.Cleanup()
	require.EqualValues(t, 0, len(pool.msgs))
}

func TestMsgPool_InsertProposal(t *testing.T) {
	pool := NewMsgPool(4, 4, 3)

	msg := maxbftpb.ConsensusMsg{
		Payload: &maxbftpb.ConsensusPayload{
			Type: maxbftpb.MessageType_PROPOSAL_MESSAGE,
			Data: &maxbftpb.ConsensusPayload_ProposalMsg{
				ProposalMsg: &maxbftpb.ProposalMsg{
					ProposalData: &maxbftpb.ProposalData{
						Level:  1,
						Height: 1,
					},
				},
			},
		},
	}
	add, err := pool.InsertProposal(&msg)
	require.True(t, add)
	require.NoError(t, err)

	add, err = pool.InsertProposal(nil)
	require.False(t, add)
	require.Error(t, err)

	// get proposal
	require.Nil(t, pool.GetProposal(3, 1))
	require.NotNil(t, pool.GetProposal(1, 1))

	// cleanup status
	pool.Cleanup()
	require.EqualValues(t, 0, len(pool.msgs))
}

func TestMsgPool_OnBlockSealed(t *testing.T) {
	pool := NewMsgPool(4, 4, 3)
	pool.OnBlockSealed(1)
	require.EqualValues(t, 0, len(pool.msgs))

	// 1. init 10 consensusRound
	for i := 1; i <= 10; i++ {
		pool.msgs[uint64(i)] = newConsensusRound(4, 3)
	}
	require.EqualValues(t, 10, len(pool.msgs))

	// 2. del 1 consensusRound
	pool.OnBlockSealed(6)
	require.EqualValues(t, 9, len(pool.msgs))

	// 3. del 8 consensusRound
	pool.OnBlockSealed(14)
	require.EqualValues(t, 1, len(pool.msgs))

	// 4. del the remaining states
	pool.OnBlockSealed(15)
	require.EqualValues(t, 0, len(pool.msgs))
}
