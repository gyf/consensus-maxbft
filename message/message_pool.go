/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package message

import (
	"fmt"
	"sync"

	"chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
)

//MsgPool manages all of consensus messages received for protocol
type MsgPool struct {
	rwMtx sync.RWMutex

	validatorNum int                        //The size of validators
	msgs         map[uint64]*consensusRound //format: [height] = [proposal&votes in each level];

	// Stores consensus information at the same height and different levels
	cachedLen     uint64 //Cache the max length of latest heights
	minVotesForQc int    //Quorum certification
}

//NewMsgPool initializes a msg pool to manage all of consensus votes for protocol
func NewMsgPool(cachedLen uint64, valNum, minVotesForQc int) *MsgPool {
	return &MsgPool{
		rwMtx: sync.RWMutex{},

		cachedLen:     cachedLen,
		validatorNum:  valNum,
		minVotesForQc: minVotesForQc,
		msgs:          make(map[uint64]*consensusRound),
	}
}

//InsertVote is an external api to cache a vote msg with given height and round
func (mp *MsgPool) InsertVote(vote *maxbft.VoteData) (bool, error) {
	mp.rwMtx.Lock()
	defer mp.rwMtx.Unlock()

	height := vote.Height
	if _, ok := mp.msgs[height]; !ok {
		mp.msgs[height] = newConsensusRound(mp.validatorNum, mp.minVotesForQc)
	}
	return mp.msgs[height].insertVote(vote)
}

//InsertProposal is an external api to cache a proposal msg with given height and round

func (mp *MsgPool) InsertProposal(msg *maxbft.ConsensusMsg) (bool, error) {
	if msg == nil || msg.Payload == nil {
		return false, fmt.Errorf("try to insert nil proposal")
	}
	mp.rwMtx.Lock()
	defer mp.rwMtx.Unlock()
	proposal := msg.GetPayload().GetProposalMsg().ProposalData
	if _, ok := mp.msgs[proposal.Height]; !ok {
		mp.msgs[proposal.Height] = newConsensusRound(mp.validatorNum, mp.minVotesForQc)
	}
	return mp.msgs[proposal.Height].insertProposal(msg)
}

//GetProposal is an external api to get a proposal with given height and round
func (mp *MsgPool) GetProposal(height uint64, round uint64) *maxbft.ConsensusMsg {
	mp.rwMtx.RLock()
	defer mp.rwMtx.RUnlock()
	if _, ok := mp.msgs[height]; !ok {
		return nil
	}
	return mp.msgs[height].getProposal(round)
}

//GetQCVotes is an external api to get votes at given height and round
func (mp *MsgPool) GetQCVotes(height uint64, round uint64) []*maxbft.VoteData {
	mp.rwMtx.RLock()
	defer mp.rwMtx.RUnlock()
	if _, ok := mp.msgs[height]; !ok {
		return nil
	}
	return mp.msgs[height].getQCVotes(round)
}

//CheckAnyVotes is an external api to check whether self have received minVotesForQc votes
func (mp *MsgPool) CheckAnyVotes(height uint64, round uint64) bool {
	mp.rwMtx.RLock()
	defer mp.rwMtx.RUnlock()
	if _, ok := mp.msgs[height]; !ok {
		return false
	}
	return mp.msgs[height].checkAnyVotes(round)
}

//CheckVotesDone is an external api to check whether self have received enough votes for a valid block or change view
func (mp *MsgPool) CheckVotesDone(height uint64, round uint64) ([]byte, bool, bool) {
	mp.rwMtx.RLock()
	defer mp.rwMtx.RUnlock()
	if _, ok := mp.msgs[height]; !ok {
		return nil, false, false
	}
	return mp.msgs[height].checkVoteDone(round)
}

//GetLastValidRound is an external api to get latest valid round at height
func (mp *MsgPool) GetLastValidRound(height uint64) int64 {
	mp.rwMtx.RLock()
	defer mp.rwMtx.RUnlock()
	if _, ok := mp.msgs[height]; !ok {
		return -1
	}
	return mp.msgs[height].getLastValidRound()
}

//OnBlockSealed is an external api to cleanup the old messages
func (mp *MsgPool) OnBlockSealed(height uint64) {
	if height <= mp.cachedLen {
		return
	}
	mp.rwMtx.Lock()
	defer mp.rwMtx.Unlock()

	toFreeHeight := make([]uint64, 0)
	for h := range mp.msgs {
		if h < height-mp.cachedLen {
			toFreeHeight = append(toFreeHeight, h)
		}
	}
	for _, h := range toFreeHeight {
		delete(mp.msgs, h)
	}
}

//Cleanup cleans up the cached messages
func (mp *MsgPool) Cleanup() {
	mp.rwMtx.Lock()
	defer mp.rwMtx.Unlock()
	mp.msgs = make(map[uint64]*consensusRound)
}

func (mp *MsgPool) UpdateMinVotesForQc(msgPool *MsgPool) {
	mp.validatorNum, mp.minVotesForQc = msgPool.validatorNum, msgPool.minVotesForQc
}
