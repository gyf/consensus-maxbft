/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package message

import (
	"testing"

	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/utils/v2"

	"github.com/stretchr/testify/require"
)

func TestCheckVoteDoneWithBlock2(t *testing.T) {
	levelVotes := newConsensusRound(4, 3)

	// add two votes by node1;
	// node1 first vote block; second vote newView
	// 1. blk = 1
	// 2. blk + newView = 2
	// 3. blk + newView = 1
	// 4. blk = 2
	// 5. blk + newView = 2
	// 6. blk + newView = 3
	blkID := []byte(utils.GetRandTxId())
	node1VoteBlk := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		BlockId:   blkID,
		AuthorIdx: 1,
	}
	node2VoteBlkAndTimeOut := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		NewView:   true,
		BlockId:   blkID,
		AuthorIdx: 2,
	}
	node1VoteBlkAndTimeOut := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		BlockId:   blkID,
		NewView:   true,
		AuthorIdx: 1,
	}
	node2VoteBlk := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		BlockId:   blkID,
		AuthorIdx: 2,
	}
	node2VoteBlkAndTimeOut2 := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		BlockId:   blkID,
		NewView:   true,
		AuthorIdx: 2,
	}
	node3VoteBlkAndTimeOut := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		BlockId:   blkID,
		NewView:   true,
		AuthorIdx: 3,
	}
	add, err := levelVotes.insertVote(node1VoteBlk)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	add, err = levelVotes.insertVote(node2VoteBlkAndTimeOut)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	add, err = levelVotes.insertVote(node1VoteBlkAndTimeOut)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// add two votes by node2;
	// node2 first vote block; second vote newView

	add, err = levelVotes.insertVote(node2VoteBlk)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")
	add, err = levelVotes.insertVote(node2VoteBlkAndTimeOut2)
	require.False(t, add, "add vote failed")
	require.NoError(t, err, "shouldn't error")

	_, _, done := levelVotes.checkVoteDone(1)
	require.False(t, done)

	add, err = levelVotes.insertVote(node3VoteBlkAndTimeOut)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	doneBlkID, isNewView, done := levelVotes.checkVoteDone(1)
	require.False(t, isNewView)
	require.True(t, done)
	require.EqualValues(t, doneBlkID, blkID)

	votes := levelVotes.getQCVotes(1)
	require.EqualValues(t, 3, len(votes))
}

func TestCheckVoteDoneWithBlock(t *testing.T) {
	levelVotes := newConsensusRound(4, 3)

	// 1. add newView vote1 with level1
	add, err := levelVotes.insertVote(&maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		NewView:   true,
		AuthorIdx: 1,
	})
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 2. add BlockId vote2 with level1
	voteBlock1 := maxbftpb.VoteData{
		Level: 1, Height: 1, AuthorIdx: 2, BlockId: []byte(utils.GetRandTxId()),
	}
	add, err = levelVotes.insertVote(&voteBlock1)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 3. add BlockId vote3 with level1
	voteBlock2 := voteBlock1
	voteBlock2.AuthorIdx = 3
	add, err = levelVotes.insertVote(&voteBlock2)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 5. add BlockId vote4 with level1
	voteBlock3 := voteBlock1
	voteBlock3.AuthorIdx = 4
	add, err = levelVotes.insertVote(&voteBlock3)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 7. check vote done should be false
	voteBlockId, voteNewView, done := levelVotes.checkVoteDone(1)
	require.True(t, done, "should be done")
	require.False(t, voteNewView, "should vote newView")
	require.EqualValues(t, voteBlockId, voteBlock2.BlockId)

	// 8. check last done level
	require.EqualValues(t, 1, levelVotes.getLastValidRound())
}

func TestCheckVoteDoneWithNewView(t *testing.T) {
	levelVotes := newConsensusRound(4, 3)

	// 1. add newView vote1 with level1
	voteNewView1 := maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		NewView:   true,
		AuthorIdx: 1,
	}
	add, err := levelVotes.insertVote(&voteNewView1)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 2. add newView vote2 with level1
	voteNewView2 := voteNewView1
	voteNewView2.AuthorIdx = 2
	add, err = levelVotes.insertVote(&voteNewView2)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 3. check vote done should be false
	_, _, done := levelVotes.checkVoteDone(1)
	require.False(t, done, "should not be done")

	// 4. add BlockId vote3 with level1
	voteBlock := &maxbftpb.VoteData{
		Level: 1, Height: 1, AuthorIdx: 3, BlockId: []byte(utils.GetRandTxId()),
	}
	add, err = levelVotes.insertVote(voteBlock)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 5. check vote done should be false
	_, _, done = levelVotes.checkVoteDone(1)
	require.False(t, done, "should not be done")

	// 6. add newView vote4 with level1
	voteNewView3 := voteNewView1
	voteNewView3.AuthorIdx = 4
	add, err = levelVotes.insertVote(&voteNewView3)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 7. check vote done should be false
	voteBlockId, voteNewView, done := levelVotes.checkVoteDone(1)
	require.True(t, done, "should be done")
	require.True(t, voteNewView, "should vote newView")
	require.Nil(t, voteBlockId, "should BlockId is null")

	// 8. check last done level
	require.EqualValues(t, 1, levelVotes.getLastValidRound())

	_, _, done = levelVotes.checkVoteDone(89)
	require.False(t, done)
}

func TestInsertVote(t *testing.T) {
	levelVotes := newConsensusRound(4, 3)

	// 1. add newView vote1 with level1
	voteNewView1 := &maxbftpb.VoteData{
		Level:     1,
		Height:    1,
		NewView:   true,
		AuthorIdx: 1,
	}
	add, err := levelVotes.insertVote(voteNewView1)
	require.True(t, add, "add vote success")
	require.NoError(t, err, "shouldn't error")

	// 2. add same vote should error
	add, err = levelVotes.insertVote(voteNewView1)
	require.False(t, add, "add vote failed")
	require.NoError(t, err, "shouldn be add  error")

	// add different vote in same level and same author
	voteBlock := &maxbftpb.VoteData{
		Level: 1, Height: 1, AuthorIdx: 1, BlockId: []byte(utils.GetRandTxId()),
	}
	add, err = levelVotes.insertVote(voteBlock)
	require.True(t, add, "add vote failed")
	require.NoError(t, err, "shouldn be add vote error")

	// 3. add vote in diff level
	voteBlock2 := &maxbftpb.VoteData{
		Level: 2, Height: 1, AuthorIdx: 1, BlockId: []byte(utils.GetRandTxId()),
	}
	add, err = levelVotes.insertVote(voteBlock2)
	require.True(t, add, "add vote succeed")
	require.NoError(t, err, "nil error because add success")
}

func TestInsertProposal(t *testing.T) {
	levelVotes := newConsensusRound(4, 3)

	// each Consensus Level allows only one proposal to be added

	// 1. add first proposal in level1
	msg := &maxbftpb.ConsensusMsg{
		Payload: &maxbftpb.ConsensusPayload{
			Type: maxbftpb.MessageType_PROPOSAL_MESSAGE,
			Data: &maxbftpb.ConsensusPayload_ProposalMsg{
				ProposalMsg: &maxbftpb.ProposalMsg{
					ProposalData: &maxbftpb.ProposalData{
						Level: 1,
					},
				},
			},
		},
	}
	add, err := levelVotes.insertProposal(msg)
	require.True(t, add, "add proposal success")
	require.NoError(t, err, "shouldn't error")

	// 2. add diff proposal in same level
	add, err = levelVotes.insertProposal(msg)
	require.False(t, add, "add proposal failed")
	require.Error(t, err, "shouldn be add proposal error")

	// 3. get proposal
	require.NotNil(t, levelVotes.getProposal(1))
	require.Nil(t, levelVotes.getProposal(3))
}

func TestCheckVoteAny(t *testing.T) {
	levelVotes := newConsensusRound(4, 3)

	for i := 0; i < 3; i++ {
		voteBlock := &maxbftpb.VoteData{
			Level: 1, Height: 1, AuthorIdx: uint64(i + 1), NewView: true,
		}
		_, _ = levelVotes.insertVote(voteBlock)
	}

	// 1. should have anyVote in level 1
	require.True(t, levelVotes.checkAnyVotes(1))
	require.False(t, levelVotes.checkAnyVotes(89))

	// 2. del one vote, not have anyVote in level 1
	levelVotes.delVote(&maxbftpb.VoteData{
		Level: 1, Height: 1, AuthorIdx: 1, NewView: true,
	})
	require.False(t, levelVotes.checkAnyVotes(1))
}
