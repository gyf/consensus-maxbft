/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/
package message

import (
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
)

//consensusRound caches consensus msg per consensus's level
type consensusRound struct {
	validatorNum  int //the number of validators
	minVotesForQc int //the min quorum for vote

	votes     map[uint64]*votePool            //format: [level] = votePool;
	proposals map[uint64]*maxbft.ConsensusMsg //format: [level] = ConsensusMsg; only store proposal msg
}

//newConsensusRound initializes a consensus level with given params
func newConsensusRound(size, minVotesForQc int) *consensusRound {
	return &consensusRound{
		validatorNum:  size,
		minVotesForQc: minVotesForQc,

		votes:     make(map[uint64]*votePool),
		proposals: make(map[uint64]*maxbft.ConsensusMsg),
	}
}

//checkAnyVotes checks whether self have received any minVotesForQc votes with given level and voteType
func (cr *consensusRound) checkAnyVotes(level uint64) bool {
	pool, ok := cr.votes[level]
	if !ok {
		return false
	}
	return len(pool.votes) >= cr.minVotesForQc
}

//insertVote inserts a vote msg to vote pool
func (cr *consensusRound) insertVote(vote *maxbft.VoteData) (bool, error) {
	if _, ok := cr.votes[vote.Level]; !ok {
		cr.votes[vote.Level] = newVotePool(cr.validatorNum)
	}
	pool := cr.votes[vote.Level]
	return pool.insertVote(vote, cr.minVotesForQc)
}

//delVote delete the vote from votePool
func (cr *consensusRound) delVote(vote *maxbft.VoteData) {
	pool, ok := cr.votes[vote.Level]
	if !ok {
		return
	}
	pool.delVote(vote)
}

//insertProposal inserts a proposal to proposal list
func (cr *consensusRound) insertProposal(msg *maxbft.ConsensusMsg) (bool, error) {
	proposal := msg.Payload.GetProposalMsg()
	level := proposal.ProposalData.Level
	if _, ok := cr.proposals[level]; ok {
		return false, fmt.Errorf("duplicated proposal message")
	}
	cr.proposals[level] = msg
	return true, nil
}

//getProposal returns a proposal at level
func (cr *consensusRound) getProposal(level uint64) *maxbft.ConsensusMsg {
	return cr.proposals[level]
}

//getVotes returns all of votes at given level
func (cr *consensusRound) getQCVotes(level uint64) []*maxbft.VoteData {
	if _, ok := cr.votes[level]; !ok {
		return nil
	}
	return cr.votes[level].getQCVotes()
}

//getLastValidRound returns the latest valid level at which enough votes received
func (cr *consensusRound) getLastValidRound() int64 {
	lastValidRound := int64(-1)
	for level := range cr.votes {
		_, _, done := cr.checkVoteDone(level)
		if done && lastValidRound < int64(level) {
			lastValidRound = int64(level)
		}
	}
	return lastValidRound
}

//checkVoteDone checks whether self have received enough votes with given vote type at level
func (cr *consensusRound) checkVoteDone(level uint64) (blkID []byte, isNewView bool, done bool) {
	if _, ok := cr.votes[level]; !ok {
		return nil, false, false
	}
	return cr.votes[level].checkVoteDone()
}
