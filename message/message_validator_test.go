/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package message

import (
	"testing"

	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	maxbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/maxbft"
	"chainmaker.org/chainmaker/utils/v2"

	"github.com/stretchr/testify/require"
)

func TestValidateMessageBasicInfo(t *testing.T) {
	proposalMsg := maxbftpb.ConsensusPayload{
		Type: maxbftpb.MessageType_PROPOSAL_MESSAGE,
		Data: &maxbftpb.ConsensusPayload_ProposalMsg{
			ProposalMsg: &maxbftpb.ProposalMsg{
				ProposalData: &maxbftpb.ProposalData{
					Level:  1,
					Height: 1,
					Block: &common.Block{
						Header: &common.BlockHeader{
							BlockHash:    []byte(utils.GetRandTxId()),
							PreBlockHash: []byte(utils.GetRandTxId()),
							Signature:    []byte("signature"),
							Proposer:     &accesscontrol.Member{},
						},
					},
					Proposer:  []byte("proposer"),
					JustifyQc: &maxbftpb.QuorumCert{},
				},
				SyncInfo: &maxbftpb.SyncInfo{
					HighestQc: &maxbftpb.QuorumCert{},
				},
			},
		},
	}
	require.NoError(t, ValidateMessageBasicInfo(&proposalMsg))

	voteMsg := maxbftpb.ConsensusPayload{
		Type: maxbftpb.MessageType_VOTE_MESSAGE,
		Data: &maxbftpb.ConsensusPayload_VoteMsg{
			VoteMsg: &maxbftpb.VoteMsg{
				VoteData: &maxbftpb.VoteData{
					Level:   1,
					NewView: true,
					Author:  []byte("author"),
				},
				SyncInfo: &maxbftpb.SyncInfo{
					HighestQc: &maxbftpb.QuorumCert{},
				},
			},
		},
	}
	require.NoError(t, ValidateMessageBasicInfo(&voteMsg))

	fetchMsg := maxbftpb.ConsensusPayload{
		Type: maxbftpb.MessageType_BLOCK_FETCH_MESSAGE,
		Data: &maxbftpb.ConsensusPayload_BlockFetchMsg{
			BlockFetchMsg: &maxbftpb.BlockFetchMsg{
				BlockId: []byte(utils.GetRandTxId()),
			},
		},
	}
	require.NoError(t, ValidateMessageBasicInfo(&fetchMsg))

	fetchRespMsg := maxbftpb.ConsensusPayload{
		Type: maxbftpb.MessageType_BLOCK_FETCH_RESP_MESSAGE,
		Data: &maxbftpb.ConsensusPayload_BlockFetchRespMsg{
			BlockFetchRespMsg: &maxbftpb.BlockFetchRespMsg{
				Blocks: []*maxbftpb.BlockPair{},
			},
		},
	}
	require.NoError(t, ValidateMessageBasicInfo(&fetchRespMsg))
}
